﻿using System;

namespace Strings
{
    class Strings
    {
        /**Autor: Iulia Todor
         * Data: 12/12/2022
         * Descripció:Feu un programa que rebi per l’entrada 2 seqüències de caràcters i digui si aquestes són 
         * iguals.**/
        public static void SonIguals()
        {
            Console.WriteLine("Dame dos palabras y te diré si son iguales o no");

            Console.WriteLine("Primera palabra: ");
            string? palabra1 = Console.ReadLine();
            string? palabra2 = "a";

            //Por si la segunda palabra tiene diferente longitud a la primera
            do
            {
                Console.WriteLine("Recuerda que la segunda palabra tiene que ser igual de larga que la primera");
                Console.WriteLine("Segunda palabra: ");
                palabra2 = Console.ReadLine();
            } while (palabra2?.Length != palabra1?.Length);

            int value = 0;

            //Si hay alguna letra que sea diferente entre los dos string, valor aumentará. Las palabras
            //serán iguales si al final valor es igual a 0.
            for (int i = 0; i < palabra1?.Length; i++)
            {
                if (palabra1[i] != palabra2?[i])
                { value++; }
            }

            if (value == 0)
            { Console.WriteLine("Son iguales"); }

            else
            { Console.WriteLine("No son iguales"); }

        }
        /**Autor: Iulia Todor
         * Data: 12/12/2022
         * Descripció:Feu un programa que rebi per l’entrada 2 seqüències de caràcters i digui 
         * si aquestes són iguals, sense tenir en compte majúscules i minúscules.**/
        public static void SonIguals2()
        {
            Console.WriteLine("Dame dos palabras y te diré si son iguales o no");

            Console.WriteLine("Primera palabra: ");
            string? palabra1 = Console.ReadLine();
            string? palabra2 = "a";

            //Por si la segunda palabra tiene diferente longitud a la primera
            do
            {
                Console.WriteLine("Recuerda que la segunda palabra tiene que ser igual de larga que la primera");
                Console.WriteLine("Segunda palabra: ");
                palabra2 = Console.ReadLine();
            } while (palabra2?.Length != palabra1?.Length);

            int value = 0;

            //Hacemos que las dos palabras estén en mayúscula para que no haya inconvenientes entre mayúsculas
            //y minúsculas
            palabra1 = palabra1?.ToUpper();
            palabra2 = palabra2?.ToUpper();

            //Si hay alguna letra que sea diferente entre los dos string, valor aumentará. Las palabras
            //serán iguales si al final valor es igual a 0.
            for (int i = 0; i < palabra1?.Length; i++)
            {
                if (palabra1[i] != palabra2?[i])
                {
                    value++;
                }
            }

            if (value == 0)
            { Console.WriteLine("Son iguales"); }

            else
            { Console.WriteLine("No son iguales"); }

        }
        /**Autor: Iulia Todor
         * Data: 12/12/2022
         * Descripció:Feu un programa que rebi per l’entrada 1 String i després una serie indefinida de caràcters per anar traient de l’String, si és que el conté, fins que es rep un 0.
        Entrada
        L’entrada consisteix en un String seguit de caràcters individuals, fins a la introducció d’un 0.
        Sortida
        Per la sortida heu d’imprimir l’String resultat de treure els caràcters.
        **/
        public static void PurgaDeCaracters()
        {
            Console.WriteLine("Escribe una palabra");

            string? palabra = Console.ReadLine()?.ToUpper();

            Console.WriteLine("Ahora dime qué carácteres quieres purgar de la palabra");

            //Estos serán los carácteres que el usuario escogerá para purgar
            char purga = '+';


            int valor = 0;

            do
            {
                purga = Convert.ToChar(Console.ReadLine().ToUpper());

                //valor aumenta para saber si purga está dentro de la palabra. De lo contrario no la eliminará
                for (int i = 0; i < palabra?.Length; i++)
                {
                    if (purga == palabra[i])
                    { valor++; }

                }

                if (valor == 0)
                {   //Para poder salir del bucle si el usuario introduce 0
                    if (purga == '0')
                    { break; }

                    else
                    { Console.WriteLine("Recuerda que el carácter tiene que estar en la palabra"); }

                }
                //Si purga está dentro de la palabra será eliminado de esta
                else if (valor > 0)
                {
                    //\0 es un valor nulo
                    palabra = palabra?.Replace(purga, '\0');
                    valor = 0;
                }

            } while (purga != '0');

            Console.WriteLine("Acabado");
            Console.WriteLine(palabra);
        }
        /**Autor: Iulia Todor
         * Data: 12/12/2022
         * Descripció:Feu un programa que rebi per l’entrada 1 seqüència de caràcters i just després dos caràcters, el primer per substituir de l’String pel segon.
        Entrada
        L’entrada consisteix en un String i dos caràcters.
        Sortida
        Per la sortida heu d’imprimir l’String amb el caràcter reemplaçat.
        **/
        public static void SubstitueixElCaracter()
        {

            Console.WriteLine("Escribe una palabra");

            string? palabra = Console.ReadLine()?.ToUpper();

            char purgado = '+';

            char sustitucion = '-';

            int valor = 0;

            do
            {
                Console.WriteLine("Dime qué carácter quieres sustituir en la palabra");
                //purgado es el carácter que será sustituido de la palabra 
                purgado = Convert.ToChar(Console.ReadLine().ToUpper());

                Console.WriteLine("Dime por qué carácter quieres sustituirlo");
                //sustitucion es el carácter por el que sustituiremos purgado
                sustitucion = Convert.ToChar(Console.ReadLine().ToUpper());

                //valor aumenta para saber si purgado está dentro de la palabra. De lo contrario no la sustituirá
                for (int i = 0; i < palabra.Length; i++)
                {
                    if (purgado == palabra[i])
                    { valor++; }
                }

                if (valor == 0)
                {    //Para poder salir del bucle si el usuario introduce 0
                    if (purgado == '0')
                    { break; }

                    else
                    { Console.WriteLine("Recuerda que el carácter purgado tiene que estar en la palabra"); }

                }
                //Si purgado está dentro de la palabra este será eliminado
                else if (valor > 0)
                {
                    palabra = palabra.Replace(purgado, sustitucion);
                    valor = 0;
                }


            } while (purgado != '0' || sustitucion != '0');

            Console.WriteLine("Acabado");
            Console.WriteLine(palabra);

        }
        /**Autor: Iulia Todor
         * Data: 12/12/2022
         * Descripció:Feu un programa que rebi per l’entrada 2 seqüències d’ADN i calculi la distància d’Hamming entre elles. Tingueu en compte que per poder realitzar aquest càlcul heu d’assegurar que les cadenes tinguin exactament la mateixa mida.
        Entrada
        L’entrada consisteix en dues seqüències d’ADN en forma de 2 Strings.
        Sortida
        Per la sortida heu d’imprimir el número que representa la distància d’Hamming o indicar que l’entrada no és vàlida si les seqüències no són de la mateixa mida.
        **/
        public static void DistanciaHamming()
        {
            Console.WriteLine("Escribe la primera secuencia");

            string? secuencia1 = Console.ReadLine();
            string? secuencia2 = Console.ReadLine();

            int hamming = 0;
            //Por si las secuencias son de diferente tamaño
            if (secuencia1?.Length != secuencia2?.Length)
            { Console.WriteLine("Secuencia no válida"); }

            else
            {
                //Si hay carácteres en diferentes posiciones hamming aumenta, y esta acabará siendo la distancia
                for (int i = 0; i < secuencia1?.Length; i++)
                {
                    if (secuencia1[i] != secuencia2?[i])
                    { hamming++; }
                }

                if (hamming > 0)
                { Console.WriteLine("La distancia de hamming es: " + hamming); }

            }


        }
        /**Autor: Iulia Todor
         * Data: 12/12/2022
         * Descripció:Feu una aplicació que donada una seqüència amb només ‘(’ i ‘)’, digueu si els parèntesis tanquen correctament.
        Entrada
        L’entrada consisteix en una seqüència amb només ‘(’ i ‘)’.
        Sortida
        Per la sortida heu d’imprimir si els parèntesis tanquen bé o no.
        **/
        public static void Parentesis()
        {
            Console.WriteLine("Introduce los paréntesis");
            //válido variará según si el paréntesis es abierto o cerrado. Para que el string sea
            //válido, la variable válido tiene que ser 0 para el final del ciclo.
            int valido = 0;

            string? parentesis = Console.ReadLine();

            for (int i = 0; i < parentesis?.Length; i++)
            {
                //si el paréntesis es cerrado, valido disminuirá
                if (parentesis[i] == ')')
                {
                    valido--;
                    //Esto es para que no se puedan poner secuencias como ))((. A la que empieces con un paréntesis
                    //cerrado el programa dirá que es inválido
                    if (valido < 0)
                    { break; }
                }
                //si el paréntesis es abierto, valido aumentará
                else if (parentesis[i] == '(')
                { valido++; }

            }

            if (valido == 0)
            { Console.WriteLine("Valido"); }

            else
            { Console.WriteLine("No valido"); }


        }
        /**Autor: Iulia Todor
         * Data: 12/12/2022
         * Descripció:Feu un programa que indiqui si una paraula és un palíndrom o no. Recordeu que una paraula és un palíndrom si es llegeix igual d’esquerra a dreta que de dreta a esquerra.
        Entrada
        L’entrada consisteix en un String.
        Sortida
        Per la sortida heu d’imprimir si la paraula és un palíndrom o no.
        **/
        public static void Palindrom()
        {
            Console.WriteLine("Introduce una palabra y te dire si es palindromo");

            //num será una variable que empieza siendo 0 y aumentará en caso de que haya indicios de que la
            //palabra no es un palindromo. Si para el final num sigue siendo 0, significa que la palabra
            //es un palindromo
            int num = 0;

            string? palabra = Console.ReadLine();
            //Para evitar problemas entre mayusculas y mayusculas
            palabra = palabra?.ToUpper();


            for (int i = 0; i < palabra?.Length; i++)
            {
                if (palabra[i] != palabra[palabra.Length - i - 1])
                { num++; }
            }

            if (num == 0)
            { Console.WriteLine(true); }

            else
            { Console.WriteLine(false); }

        }
        /**Autor: Iulia Todor
         * Data: 12/12/2022
         * Descripció:Feu un programa que llegeixi paraules, i que escrigui cadascuna invertint l’ordre dels seus caràcters.
        Entrada
        L’entrada consisteix en diverses paraules.
        Sortida
        Escriviu cada paraula, invertida, en una línia.
        **/
        public static void InverteixParaules()
        {
            Console.WriteLine("Vamos a jugar a un juego: introduce palabras y te la voy a invertir. Introduce 0 para terminar");
            string? palabra = "a";
            do
            {
                Console.WriteLine("Introduce una palabra: ");

                palabra = Console.ReadLine();

                //Dividimos la palabra
                for (int i = 0; i < palabra?.Length; i++)
                { palabra.Split(palabra[i]); }
                //Metemos la palabra dentro de un array
                char[] inverse = palabra.ToCharArray();

                //Invertimos el array
                Array.Reverse(inverse);

                for (int i = 0; i < inverse.Length; i++)
                { Console.Write(inverse[i]); }
                Console.WriteLine();


            } while (palabra != "0");

        }
        /**Autor: Iulia Todor
         * Data: 12/12/2022
         * Descripció:Feu un programa que llegeixi un String i inverteixi l’ordre de les paraules dins del mateix.
        Entrada
        L’entrada consisteix en un String.
        Sortida
        Escriviu l’String amb les seves paraules invertides.
        **/
        public static void InverteixParaules2()
        {
            Console.WriteLine("Introduce una frase y te la voy a invertir. Introduce 0 para terminar");
            string? palabra = "a";
            do
            {
                Console.WriteLine("Introduce la frase: ");

                palabra = Console.ReadLine();

                //Separamos los string en los espacios, los juntamos y los invertimos
                string result = String.Join(" ", palabra.Split(' ').Reverse());

                for (int i = 0; i < palabra.Length; i++)
                {Console.Write(result[i]);}

                Console.WriteLine();

            } while (palabra != "0");


        }
        /**Autor: Iulia Todor
         * Data: 12/12/2022
         * Descripció:Feu un programa que llegeixi una seqüència de lletres acabada en punt i digui si conté la successió de lletres consecutives ‘h’,‘o’, ‘l’, ‘a’ o no.
        Entrada
        L’entrada consisteix en una seqüència de lletres minúscules acabada en ‘.’.
        Sortida
        Cal escriure “hola” si l’entrada conté les lletres ‘h’, ‘o’, ‘l’, ‘a’ consecutivament. Altrament, cal escriure “adeu”.
        **/
        public static void HolaIAdeu()
        {
            Console.WriteLine("Si escribes una palabra te diré si contiene de forma consecutiva la palabra hola");

            string? palabra = "a";

            do
            {
                Console.WriteLine("Introduce la palabra: ");
                
                palabra = Console.ReadLine();
                //Si el string contiene hola de forma consecutiva, holacounter aumenta.
                int holacounter = 0;
                //Si la palabra es diferente a 0. Para que no imprima ni hola ni adiós
                if (palabra != "0")
                {
                    for (int i = 0; i < palabra?.Length; i++)
                    {
                        if (palabra.Contains("hola"))
                        {holacounter++;}
                    }
                    //Si la palabra contiene "hola", imprime hola. Si no imprime adiós
                    if (holacounter > 0)
                    {Console.WriteLine("hola");}

                    else
                    {Console.WriteLine("adiós");}

                    Console.WriteLine();
                }

            } while (palabra != "0");

            Console.WriteLine("¡Nos vemos!");

        }


        public static void Menu()
        {
            string? opcio;
            do
            {
                Console.WriteLine("0. Sortir");
                Console.WriteLine("1. Són iguals?");
                Console.WriteLine("2. Són iguals? (2)");
                Console.WriteLine("3. Purga de caràcters");
                Console.WriteLine("4. Substitueix el caràcter");
                Console.WriteLine("5. Distància d'Hamming");
                Console.WriteLine("6. Parèntesis");
                Console.WriteLine("7. Palíndrom");
                Console.WriteLine("8. Inverteix les paraules");
                Console.WriteLine("9. Inverteix les paraules(2)");
                Console.WriteLine("10. Hola i adeu");




                opcio = Console.ReadLine();

                switch (opcio)
                {
                    case "1":
                        SonIguals();
                        break;
                    case "2":
                        SonIguals2();
                        break;
                    case "3":
                        PurgaDeCaracters();
                        break;
                    case "4":
                        SubstitueixElCaracter();
                        break;
                    case "5":
                        DistanciaHamming();
                        break;
                    case "6":
                        Parentesis();
                        break;
                    case "7":
                        Palindrom();
                        break;
                    case "8":
                        InverteixParaules();
                        break;
                    case "9":
                        InverteixParaules2();
                        break;
                    case "10":
                        HolaIAdeu();
                        break;
                    case "0":
                        Console.WriteLine("Bye");
                        break;
                    default:
                        Console.WriteLine("Opción incorrecta");
                        break;
                }

                Console.ReadLine();

            } while (opcio != "0");
        }
        static void Main()
        {
            Menu();
        }

    }
}
