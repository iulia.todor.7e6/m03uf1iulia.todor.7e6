﻿using System;
using System.ComponentModel.Design;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Security.Cryptography;

namespace Metodes
{
    class Metodes
    {
        public static void Menu()
        {
            string? opcio;
            do
            {
                Console.WriteLine("0. Sortir");
                Console.WriteLine("1. Right triangle size");
                Console.WriteLine("2. Lamp");
                Console.WriteLine("3. Camp Site Organizer");
                Console.WriteLine("4. Basic Robot");
                Console.WriteLine("5. Three in a row");
                Console.WriteLine("6. SquashCounter");

                opcio = Console.ReadLine();

                switch (opcio)
                {
                    case "1":
                        RightTriangleSize();
                        break;
                    case "2":
                        Lamp();
                        break;
                    case "3":
                        CampSiteOrganizer();
                        break;
                    case "4":
                        BasicRobot();
                        break;
                    case "5":
                        ThreeInARow();
                        break;
                    case "6":
                        SquashCounter();
                        break;
                    case "0":
                        Console.WriteLine("Bye");
                        break;
                    default:
                        Console.WriteLine("Opción incorrecta");
                        break;
                }

                Console.ReadLine();

            } while (opcio != "0");
        }
        static void Main()
        {
            Menu();
        }

        //Función para que el usuario introduzca un íntegro
        static int LlegirEnter()
        {
            int enterLlegit;
            enterLlegit = Convert.ToInt32(Console.ReadLine());
            return enterLlegit;
        }

        //Para que el usuario esté obligado a introducir un número positivo
        static void MustBePositive(double PositiveNumber)
        {

            if (PositiveNumber < 0)
            {
                Console.WriteLine("Debe ser un número positivo" + "\n");
            }

        }

        //Calcula el área de un triángulo
        static void Area(int numTriangulos, double[] areas, double[] bases, double[] alturas)
        {
            for (int i = 0; i < numTriangulos; i++)
            {
                areas[i] = (bases[i] * alturas[i]) / 2;
            }

        }
        //Calcula el perímetro de un triángulo
        static void Perimetro(int numTriangulos, double[] perimetros, double[] hipotenusa, double[] bases, double[] alturas)
        {
            for (int i = 0; i < numTriangulos; i++)
            {
                hipotenusa[i] = Math.Sqrt(Math.Pow(bases[i], 2) + Math.Pow(alturas[i], 2));
                perimetros[i] = bases[i] + alturas[i] + hipotenusa[i];
            }

        }

        /**Autor: Iulia Todor
         * Data: 10/01/2023
         * Descripció:Ens digui l'àrea i el perímetre d'una llista de triangles rectangles.
        L'usuari introdueix el número de triangles
        L'usuari entra la base i l'altura del triangle
        Imprimeix la superfície i el perímetre dels diferents triangles.
        **/
        public static void RightTriangleSize()
        {

            int numTriangulos = -2;
            double baseTriangulo = -1;
            double alturaTriangulo = -1;

            do
            {
                Console.WriteLine("Introduce el número de triángulos que quieres.");

                numTriangulos = LlegirEnter();

                if (numTriangulos < 0)
                {
                    Console.WriteLine("Debe ser un número positivo" + "\n");
                }


            } while (numTriangulos < 0);

            //Estos arrays guardan la información de los triángulos para imprimirlos al final
            double[] bases = new double[numTriangulos];
            double[] alturas = new double[numTriangulos];
            double[] areas = new double[numTriangulos];
            double[] perimetros = new double[numTriangulos];
            double[] hipotenusa = new double[numTriangulos];


            for (int i = 0; i < numTriangulos; i++)
            {
                do
                {
                    Console.WriteLine("Introduce la base del triángulo" + "[" + (i + 1) + "]");

                    baseTriangulo = LlegirEnter();

                    MustBePositive(baseTriangulo);

                } while (baseTriangulo <= 0);

                bases[i] = baseTriangulo;

                do
                {
                    Console.WriteLine("Introduce la altura del triángulo" + "[" + (i + 1) + "]");

                    alturaTriangulo = LlegirEnter();

                    MustBePositive(alturaTriangulo);




                } while (alturaTriangulo <= 0);

                alturas[i] = alturaTriangulo;

            }

            Area(numTriangulos, areas, bases, alturas);

            Perimetro(numTriangulos, perimetros, hipotenusa, bases, alturas);


            for (int i = 0; i < numTriangulos; i++)
            {
                Console.WriteLine("Un triangulo de " + bases[i] + " de base " + "y altura " + alturas[i] + " tiene " + areas[i] + " de área y " + perimetros[i] + " de perimetro");

            }




        }
        /**Autor: Iulia Todor
        * Data: 10/01/2023
        * Descripció: Simuli una làmpada, amb les accions:
                TURN ON: Encén la làmpada
                TURN OFF: Apaga la làmpada
                TOGGLE: Canvia l'estat de la làmpada

                Després de cada acció mostra si la làmpada està encesa.


       **/

        //Hace que el bool sea true
        static void boolTrue(bool changeState)
        {
            changeState = true;
            Console.WriteLine(changeState);
        }

        //Hace que el bool sea false
        static void boolFalse(bool changeState)
        {
            changeState = false;
            Console.WriteLine(changeState);
        }

        //Cambia el bool al estado contrario al que se encuentra
        static bool alterBool(bool changeState)
        {
            changeState = !changeState;
            Console.WriteLine(changeState);
            return changeState;
        }

        //Esta función pide un string al usuario y lo convierte a mayúscula.
        static string EnterUpperString()
        {
            string? enterString;
            enterString = Console.ReadLine().ToUpper();
            return enterString;
        }
        public static void Lamp()
        {
            Console.WriteLine("Tenemos una lámpara. Introduce TURN ON para encender la lámpara. TURN OFF para apagar la lámapara. TOGGLE para cambiar el estado de la lámpara");

            string stateController = "a";
            bool state = true;

            do
            {
                Console.WriteLine("\n" + "Introduce tu comando");

                stateController = EnterUpperString();

                if (stateController != "END")
                {

                    switch (stateController)
                    {
                        case "TURN ON":
                            boolTrue(state);
                            break;
                        case "TURN OFF":
                            boolFalse(state);
                            break;
                        case "TOGGLE":
                            state = alterBool(state);
                            break;
                        default:
                            Console.WriteLine("Opción incorrecta");
                            break;
                    }
                }

            } while (stateController != "END");

            Console.WriteLine("Adiós");

        }

        /** Autor: Iulia Todor
        * Data: 10/01/2023
        * Gestionar un camping. Volem tenir controlat quantes parcel·les tenim plenes i quanta gent tenim.
        Quan s'ocupa una parcel·la l'usuari introduirà el número de persones i el nom de la reserva
        ENTRA 2 Maria

        Quan es buidi una parcel·la l'usuari introduirà el nom de la reserva
        MARXA Maria

        Per tancar el programa l'usuari introduirà el nom END
        Cada cop que marxi alguna persona, imprimeix el número total de persones que hi ha, i el número de parcel·les plenes.
        **/

        //Comprueba el número de espacios en un string
        static int checkSpace(string spaceString, int spaceCounter)
        {
            for (int i = 0; i < spaceString.Length; i++)
            {
                if (spaceString[i] == ' ')
                {
                    spaceCounter++;
                }
            }
            return spaceCounter;
        }


        public static void CampSiteOrganizer()
        {
            Console.WriteLine("Vamos a organizar un camping. Introduce en una frase si la persona entra o marcha, el número de personas y el nombre de la persona a cargo de la reserva");

            string estadoReserva = "a";
            //Parcelas libres
            int parcela = 0;
            //El número de personas que entran
            int personasEntrantes = 0;
            //Número total de personas en la parcela
            int numeroPersonas = 0;
            //Array que usaremos al dividir las tres palabras que introduzca el usuario
            string[] componentesReserva;


            do
            {
                Console.WriteLine("\n" + "Introduce el estado de la reserva");

                estadoReserva = EnterUpperString();

                //Cuenta el número de espacios y asegura que haya 2. Para las tres palabras
                int spaceCounter = 0;

                spaceCounter = checkSpace(estadoReserva, spaceCounter);

                if (spaceCounter == 2)
                {
                    //Divide las tres palabras que tiene que introducir el usuario en tres strings.
                    componentesReserva = estadoReserva.Split(' ');

                    //Asegura que la segunda palabra que introduce el usuario (El número de personas) sea un íntegro.

                    try
                    {
                        personasEntrantes = int.Parse(componentesReserva[1]);

                        {

                        }
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Esto no es un número");
                    }




                    //Para obligar al usuario a introducir tres palabras y no por ejemplo " 2 " (Eso son dos espacios)
                    if (componentesReserva.Length < 3)
                    {
                        Console.WriteLine("Número de datos inválido");
                    }

                    else
                    {

                        if (componentesReserva[0] == "ENTRA")
                        {
                            parcela++;

                            //Aumenta el número de personas totales según las que hayan entrado
                            for (int i = 0; i < personasEntrantes; i++)
                            {
                                numeroPersonas++;
                            }

                            Console.WriteLine("parcelas: " + parcela);
                            Console.WriteLine("personas: " + numeroPersonas);


                        }

                        else if (componentesReserva[0] == "MARCHA")
                        {

                            //Solo resta personas si hay parcelas disponibles. No tendría sentido que se fuesen personas si no habían parcelas
                            if (parcela > 0)
                            {
                                parcela--;

                                //Para que el número de personas totales no sea negativo
                                for (int i = 0; i < personasEntrantes; i++)
                                {
                                    if (numeroPersonas >= personasEntrantes)
                                    {
                                        numeroPersonas--;
                                    }

                                    else
                                    {
                                        numeroPersonas = 0;
                                    }
                                }

                                Console.WriteLine("parcelas: " + parcela);
                                Console.WriteLine("personas: " + numeroPersonas);
                            }

                            else
                            {
                                Console.WriteLine("No es como si hubiesen parcelas en primer lugar");
                            }


                        }

                        else
                        {
                            Console.WriteLine("Estado inválido");
                        }
                    }


                }

                //Para que el usuario introduzca end sin problemas
                else if (spaceCounter < 2 && estadoReserva != "END")
                {

                    Console.WriteLine("Has introducido muy pocas palabras");

                }

                else if (spaceCounter > 2 && estadoReserva != "END")
                {

                    Console.WriteLine("Has introducido demasiadas palabras");
                }



            } while (estadoReserva != "END");


            Console.WriteLine("Adiós");


        }
        /** Autor: Iulia Todor
       * Data: 10/01/2023
       * Permet moure un robot en un planell 2D.
        El robot ha de guardar la seva posició (X, Y) i la velocitat actual. Per defecte, la posició serà (0, 0) i la velocitat és 1. La velocitat indica la distància que recorre el robot en cada acció.
        El robot té les següents accions:
        DALT: El robot es mou cap a dalt (tenint en compte la velocitat).
        BAIX: El robot es mou cap a baix (tenint en compte la velocitat).
        DRETA: El robot es mou cap a la dreta (tenint en compte la velocitat).
        ESQUERRA: El robot es mou cap a l’esquerra (tenint en compte la velocitat).
        ACELERAR: El robot aumenta en 0.5 la velocitat. La velocitat màxima és 10.
        DISMINUIR: El robot aumenta en 0.5 la velocitat. La velocitat mínima és 0.
        POSICIO: El robot imprimeix la seva posició.
        VELOCITAT: El robot imprimeix la seva velocitat.
        La simulació acaba amb l'acció END.
       **/


        //Mueve al robot hacia arriba
        static double DaltCommand(double coordenadaY, double velocitat)
        {
            coordenadaY += velocitat;
            return coordenadaY;
        }

        //Mueve el robot hacia abajo
        static double BaixCommand(double coordenadaY, double velocitat)
        {
            coordenadaY -= velocitat;
            return coordenadaY;
        }

        //Mueve el robot hacia la derecha
        static double DretaCommand(double coordenadaX, double velocitat)
        {
            coordenadaX += velocitat;
            return coordenadaX;
        }

        //Mueve el robot hacia la izquierda
        static double EsquerraCommand(double coordenadaX, double velocitat)
        {
            coordenadaX -= velocitat;
            return coordenadaX;
        }

        //Aumenta la velocidad en 0.5
        static double Acelerar(double velocidad)
        {
            velocidad += 0.5;
            return velocidad;
        }

        //Disminuye la velocidad en 0.5 si esta es mayor a 0
        static double Disminuir(double velocidad)
        {
            if (velocidad > 0)
            {
                velocidad -= 0.5;
            }
            return velocidad;
        }

        //Imprime la posición del robot
        static void Posicio(double coordenadaX, double coordenadaY)
        {
            Console.WriteLine("La posición actual es: " + "(" + coordenadaX + ", " + coordenadaY + ")");
        }

        //Imprime la velocidad del robot
        static void Velocitat(double velocidad)
        {
            Console.WriteLine("La velocidad actual es: " + velocidad);
        }

        public static void BasicRobot()
        {
            Console.WriteLine("Vamos a hacer una simulación de un robot.");
            Console.Write(" DALT: El robot es mou cap a dalt (tenint en compte la velocitat).\n BAIX: El robot es mou cap a baix (tenint en compte la velocitat).\n DRETA: El robot es mou cap a la dreta (tenint en compte la velocitat).\n ESQUERRA: El robot es mou cap a l’esquerra (tenint en compte la velocitat).\n ACELERAR: El robot aumenta en 0.5 la velocitat. La velocitat màxima és 10.\n DISMINUIR: El robot aumenta en 0.5 la velocitat. La velocitat mínima és 0.\n POSICIO: El robot imprimeix la seva posició.\n VELOCITAT: El robot imprimeix la seva velocitat.");


            string command = "a";

            double coordenadaX = 0;
            double coordenadaY = 0;
            double velocidad = 1;


            do
            {
                Console.WriteLine("\n" + "Introduce tu comando");

                command = EnterUpperString();


                if (command != "END")
                {
                    switch (command)
                    {
                        case "DALT":
                            coordenadaY = DaltCommand(coordenadaY, velocidad);
                            break;
                        case "BAIX":
                            coordenadaY = BaixCommand(coordenadaY, velocidad);
                            break;
                        case "DRETA":
                            coordenadaX = DretaCommand(coordenadaX, velocidad);
                            break;
                        case "ESQUERRA":
                            coordenadaX = EsquerraCommand(coordenadaX, velocidad);
                            break;
                        case "ACELERAR":
                            velocidad = Acelerar(velocidad);
                            break;
                        case "DISMINUIR":
                            velocidad = Disminuir(velocidad);
                            break;
                        case "POSICIO":
                            Posicio(coordenadaX, coordenadaY);
                            break;
                        case "VELOCITAT":
                            Velocitat(velocidad);
                            break;
                        default:
                            Console.WriteLine("Comando incorrecto");
                            break;
                    }
                }

            } while (command != "END");

            Console.WriteLine("(" + coordenadaX + " , " + coordenadaY + " )");

        }

        /** Autor: Iulia Todor
      * Data: 10/01/2023
     
        Jugar dos usuaris al tres en ratlla.
        Es juga amb un taulell de 3x3.Cada jugador col·loca una peça al taulell alternativament.Guanya qui és capaç de col·locar-les formant una línia de 3, ja sigui vertical, horitzontal o diagonal.
        El programa a desenvolupar haurà de mostrar el taulell per pantalla i demanar la tirada al primer jugador.
        El jugador escriurà dos números, el primer correspon a les columnes i el segon a les files. S'anirà demanant el torn alternativament a cada jugador fins que algun dels dos faci el tres en ratlla o bé fins que el taulell estigui ple.
        Les fitxes d'un jugador les representarem amb una X i les de l'altre amb una O.
        Al final es mostrarà un missatge dient el resultat de la partida: guanyen les X, guanyen les O o empat.**/

        //Imprime una matriz
        static void printMatrix(string[,] matrix)

        {

            for (int i = 0; i < matrix.GetLength(0); i++)
            {

                for (int j = 0; j < matrix.GetLength(1); j++)
                {

                    Console.Write(matrix[i, j] + "\t");
                }
                Console.WriteLine();
            }

        }
        //Esta función retorna true si un jugador gana la partida
        static bool result(bool finished, string[,] matrix, int coordenadasX, int coordenadasY, string circle)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[coordenadasX, coordenadasY] == ".")
                    {
                        matrix[coordenadasX, coordenadasY] = circle;

                        if ((matrix[i, 0] == matrix[i, 1]) && (matrix[i, 0] == matrix[i, 2]) && (matrix[i, 1] == matrix[i, 2]))
                        {
                            finished = true;

                        }

                        else if ((matrix[0, j] == matrix[1, j]) && (matrix[0, j] == matrix[2, j]) && (matrix[1, j] == matrix[2, j]))
                        {
                            finished = true;

                        }

                        else if ((matrix[0, 0] == matrix[1, 1]) && (matrix[0, 0] == matrix[2, 2]) && (matrix[1, 1] == matrix[2, 2]))

                        {
                            finished = true;

                        }

                        else if ((matrix[0, 2] == matrix[1, 1]) && (matrix[0, 2] == matrix[2, 0]) && (matrix[1, 1] == matrix[2, 0]))

                        {
                            finished = true;

                        }


                    }

                }
            }

            return finished;
        }


        //El juego de tres en raya para cada jugador
        static string[,] playerTurn(string spaceString, int spaceCounter, string[] splitCoordenadas, string[,] matrix, int coordenadasX, int coordenadasY, string circle, string playerName, bool finished)
        {

            Console.WriteLine("Turno del " + playerName);

            spaceString = EnterUpperString();

            spaceCounter = checkSpace(spaceString, spaceCounter);

            //Para que el total de espacios sea 1. Para las coordenadas.
            if (spaceCounter == 1)
            {

                splitCoordenadas = spaceString.Split(' ');

                if (splitCoordenadas[0] == "0" || splitCoordenadas[0] == "1" || splitCoordenadas[0] == "2" || splitCoordenadas[1] == "0" || splitCoordenadas[1] == "1" || splitCoordenadas[1] == "2")
                {
                    try
                    {
                        coordenadasX = int.Parse(splitCoordenadas[0]);
                        coordenadasY = int.Parse(splitCoordenadas[1]);

                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Esto no es un número");
                    }

                    if (splitCoordenadas.Length < 2 && splitCoordenadas.Length < 2)
                    {
                        Console.WriteLine("Número de datos inválido");
                    }

                    else
                    {

                        if (finished == result(finished, matrix, coordenadasX, coordenadasY, circle))
                        {

                            finished = result(finished, matrix, coordenadasX, coordenadasY, circle);

                        }

                        printMatrix(matrix);

                    }

                }

                else
                {
                    Console.WriteLine("¿No sabes jugar al tres en raya o qué?");
                }


            }

            else
            {

                Console.WriteLine("Comando inválido");

            }

            return matrix;
        }

        static bool Empate(bool draw, string[,] matrix, int coordenadasX, int coordenadasY, string circle)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] == circle)
                        draw = true;
                }
            }

            return draw;
        }


        public static void ThreeInARow()
        {
            Console.WriteLine("Vamos a jugar al tres en raya");

            string[,] matrix = new string[3, 3] { { ".", ".", "." }, { ".", ".", ".", }, { ".", ".", ".", } };
            string inputCoordenadasJ1 = "a";
            string inputCoordenadasJ2 = "b";

            int coordenadasXJ1 = 0;
            int coordenadasYJ1 = 0;
            int coordenadasXJ2 = 0;
            int coordenadasYJ2 = 0;

            string[] splitCoordenadasJ1 = new string[2];
            string[] splitCoordenadasJ2 = new string[2];

            int spaceCounter = 0;

            string circle = "0";
            string cross = "X";

            string jugador1 = "jugador 1";
            string jugador2 = "jugador 2";

            bool finished = false;

            do
            {
                playerTurn(inputCoordenadasJ1, spaceCounter, splitCoordenadasJ1, matrix, coordenadasXJ1, coordenadasYJ1, circle, jugador1, finished);

                if (finished == true)
                {
                    Console.WriteLine("Ha ganado" + jugador1);
                }

                else
                {

                    if (finished == Empate(finished, matrix, coordenadasXJ1, coordenadasYJ1, circle))
                    {
                        Console.WriteLine("Hay empate");
                    }

                    else
                    {

                        {
                            playerTurn(inputCoordenadasJ2, spaceCounter, splitCoordenadasJ2, matrix, coordenadasXJ2, coordenadasYJ2, cross, jugador2, finished);
                        }

                    }

                }

            } while (finished != true);



        }

        /** Autor: Iulia Todor
         * Data: 12/01/2023
         * Descripció: Volem fer un marcador per un partit de squash.
            Encara que hi ha diferents formes de puntuar, una de les més utilitzades pels jugadors amateurs és jugar a el millor de cinc sets. El jugador que abans guanyi tres sets guanya el partit (i en aquest moment, òbviament, aquest acaba). Els sets es juguen a 9 punts amb tres matisacions:
            Cal guanyar amb un avantatge de al menys dos punts, per tant, si s'arriba a un empat a 8, guanyarà el que arribi a 10. Si a l'intentar-ho s'empata a 9, es jugarà a 11, etc.
            Els punts només pugen a el marcador si el que guanya el punt tenia el servei. És a dir, s'utilitza el que es coneix com "recuperació de la sacada". Si el jugador A saca i guanya el punt, aquest puja al seu marcador. Però si el punt el guanya B, atès que no va ha sacat ell, recupera el servei però no puntua.
            El jugador que guanya un set comença sacant en el següent.
            Ens demanen esbrinar el marcador final de cada un dels sets d'un partit d'esquaix coneixent els guanyadors de tots els punts. És possible que el partit no acabi a causa de que el lloguer de la pista arriba a la seva fi. En aquest cas es presentarà el resultat parcial.
            L'usuari primer introduirà el número de partits a introduir.
            De cada partit, escriurà qui guanya cada punt i finalitzarà amb una F.**/


        public static void SquashCounter()
        {
            Console.WriteLine("Vamos a jugar al Squash. A representa al jugador 1 y B representa al jugador 2. Pon F al final de cada set para guardarlo.");
            int partidos = -1;
            int wonByA = 0;
            int wonByB = 0;
            int FCounter = 0;

            int gamesCounter = 0;

            int rightLetterCounter = 0;


            string result = "c";


            bool rightLetters = false;
            bool notLastF = false;

            do
            {
                Console.WriteLine("Introduce el número de partidos jugados");
                partidos = LlegirEnter();
                MustBePositive(partidos);

            } while (partidos < 0);



            do
            {

                do
                {
                    Console.WriteLine("\n" + "Introduce los resultados.");

                    result = EnterUpperString();

                    //Para comprobar que las letras sean las adecuadas
                    for (int i = 0; i < result.Length; i++)
                    {
                        if (result[i] != 'A' && result[i] != 'B' && result[i] != 'F')
                        {
                            rightLetterCounter++;
                        }
                    }

                    if (rightLetterCounter != 0)
                    {
                        Console.WriteLine("Introduce carácteres correctos");
                        rightLetterCounter = 0;
                    }

                    //Este bool es necesario para salir del ciclo y se cumplirá cuando el usuario introduzca
                    //las letras correctas
                    else
                    {
                        rightLetters = true;

                    }

                } while (rightLetters != true);



                for (int i = 0; i < result.Length; i++)
                {
                    //Si la cantidad de A es mayor que B, alguna es mayor o igual a 9
                    //y su diferencia con B es mayor o igual a 2, se imprimirá el valor de A y B.
                    //El mismo caso para B
                    if (result[i] == 'A')
                    {
                        if (wonByA > wonByB)
                        {
                            if (wonByA >= 9 || wonByB >= 9)
                            {
                                if (wonByA - wonByB >= 2 || wonByB - wonByA >= 2)
                                {
                                    Console.Write(wonByA + "-" + wonByB + "\t");
                                    wonByA = 0;
                                    wonByB = 0;
                                }
                            }
                        }

                        wonByA++;

                    }

                    else if (result[i] == 'B')
                    {
                        if (wonByA >= 9 || wonByB >= 9)
                        {
                            if (wonByA - wonByB >= 2 || wonByB - wonByA >= 2)
                            {

                                Console.Write(wonByA + "-" + wonByB + "\t");
                                wonByB = 0;
                                wonByA = 0;

                            }
                        }

                        wonByB++;

                    }



                    else if (result[i] == 'F')
                    {
                        //La cantidad de juegos aumenta si la F está al final
                        if (i == result.Length - 1)
                        {
                            gamesCounter++;
                        }
                        else
                        {
                            Console.WriteLine("La F solo se pone al acabar y me la has puesto en todo el medio");
                            Console.WriteLine("Juego inválido");
                            notLastF = true;
                            break;
                        }
                    }



                }

                //Por si el usuario no ha introducido una F al final
                for (int i = 0; i < result.Length; i++)
                {
                    if (result[i] == 'F')
                    {
                        FCounter++;
                    }
                }


                //Si el usuario introdujo la F en el lugar correcto se imprimirán los valores de A y B
                if (notLastF == false)
                {

                    Console.WriteLine(wonByA + "-" + wonByB);

                }

                if (FCounter == 0)
                {
                    Console.WriteLine("\n" + "No has escrito F al final. El juego no se guardará");
                }


                wonByA = 0;
                wonByB = 0;
                notLastF = false;


            //El bucle acaba cuando los juegos son iguales al número de partidos que ha introducido el usuario
            } while (gamesCounter != partidos);

            Console.WriteLine("Terminado");


        }
    }
}

