﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Fes un programa que rebi tres nombres enters i torni si són iguals o no**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe el primer número");

        //variables
        double i = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Ahora el segundo");
        double j = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Y el tercero");
        double k = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Te diré si son iguales o no");

        //los tres tienen que ser iguales para que sea true
        bool resultat = i == j && j == k && k == j;

        //resultado
        Console.WriteLine(resultat);




    }
}