﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 *Descripció: Llegeix el diàmetre d'una pizza rodona i imprimeix la seva superfície. Pots usar Math.PI per
escriure el valor de Pi.**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Dime el diámetro de la pizza y te diré su superficie");

        //variable 
        double diametro = Convert.ToDouble(Console.ReadLine());

        //resultado
        Console.WriteLine("El diámetro de la pizza es " + Math.Pow((diametro / 2), 2) * Math.PI);
    }
}