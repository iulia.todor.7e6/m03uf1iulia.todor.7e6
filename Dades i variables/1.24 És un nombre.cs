﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Fes un programa que rebi un caràcter i digui si és un nombre o no.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un carácter y te diré si es un número o no");

        //variable
        char ascii = Convert.ToChar(Console.ReadLine());

        //la variable se transforma en un íntegro, el cual sería su código ascii. Tiene
        //que estar comprendida entre aquellos carácteres que en la tabla de código ascii
        //van antes de los números o después de estos
        bool result = (int)ascii > (int)'/' && (int)ascii < (int)':';

        //resultado
        Console.WriteLine(result);

    }
}