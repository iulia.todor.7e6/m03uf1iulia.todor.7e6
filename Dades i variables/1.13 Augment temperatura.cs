﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Escriu un programa que llegeixi un una temperatura i un augment d’aquest, el programa ha
d’imprimir per pantalla quina és la temperatura amb l’augment aplicat.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe la temperatura que hacía antes");

        //variables
        double temp, aumento;

        temp = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Ahora el aumento que ha habido");
        aumento = Convert.ToDouble(Console.ReadLine());

        //resultado
        Console.WriteLine("La temperatura actual es " + (temp + aumento));



    }
}