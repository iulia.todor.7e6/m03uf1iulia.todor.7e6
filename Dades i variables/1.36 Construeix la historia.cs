﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Escriu un programa que llegeixi 3 frases diferents per terminal i en faci 1 història**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe tres frases y te las convertiré en una sola");

        //variables
        string frase1 = Convert.ToString(Console.ReadLine());
        string frase2 = Convert.ToString(Console.ReadLine());
        string frase3 = Convert.ToString(Console.ReadLine());

        //resultado
        Console.WriteLine(frase1 + " " + frase2 + " " + frase3);

    }
}