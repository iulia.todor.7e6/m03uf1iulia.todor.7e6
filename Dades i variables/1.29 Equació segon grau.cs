﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Fes un programa que resolgui la fórmula d’una equació de segon grau**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe tres números y vamos a volver a resolver las ecuaciones de segundo grado de la ESO");

        //variables
        double a = Convert.ToDouble(Console.ReadLine());
        double b = Convert.ToDouble(Console.ReadLine());
        double c = Convert.ToDouble(Console.ReadLine());

        //calculamos el discrimanente y la raíz por separado para no hacer el cálculo 
        //final menos horrible a la vista
        double discriminante = ((b * b) + (-4 * (a) * (c)));
        double raiz = Math.Sqrt(discriminante);

        //hay dos resultados para la parte positiva y negativa
        double result = (-b + raiz) / (2 * a);
        double result2 = (-b - raiz) / (2 * a);

        //resultados
        Console.WriteLine(result);
        Console.WriteLine(result2);



    }
}