﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Escriu un programa que donat dos números retorni la suma d’aquests.**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce dos números y te los sumaré");

        //variables
        int num = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Ahora el segundo número");

        int num2 = Convert.ToInt32(Console.ReadLine());

        //suma
        int resultat = num + num2;
        
        //resultado
        Console.WriteLine("La suma de " + num + " y " + num2 + " es " + resultat);

    }
}