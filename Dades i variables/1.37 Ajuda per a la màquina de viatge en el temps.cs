﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 *Després de molt d'esforç hem aconseguit dissenyar una màquina del temps. Només ens falta una
utilitat, necessitem un petit programa que ens indiqui en quin dia estem. Fes un programa que
imprimeixi per pantalla el següent missatge Avui és: 2021 - 09 - 17.**/

internal class Program
{
    private static void Main(string[] args)
    {
        //variable
        DateTime localdate = DateTime.Now;

        //resultado
        Console.WriteLine("Avui es:" + localdate);
    }
}