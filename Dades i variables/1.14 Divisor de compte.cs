﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Fes un programa on, introduit el número de començals i el preu d'un sopar (que pot contenir
cèntims), imprimeixi quan haurà de pagar cada començal.**/


internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el precio de la cena");
        //variables
        double precio = Convert.ToDouble(Console.ReadLine());
        Console.WriteLine("Ahora el número de personas");
        int nºpersonas= Convert.ToInt32(Console.ReadLine());
        //resultado
        Console.WriteLine("Cada persona tendrá que pagar: " + (precio / nºpersonas) + "$");

    }
}