﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 *Descripció: Llegeix el preu original i el preu actual i imprimeix el descompte (en %).**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Que la màquina del Carrefour no va? No pasa nada, dime lo que te ha costado eso");

        //double
        double num = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Ahora dime cuanto te ha costado con el descuento");
        double numD = Convert.ToDouble(Console.ReadLine());

        //cálculo
        double Descuento = (100 - ((100 * numD) / num));

        //reusltado
        Console.WriteLine("Te han descontado " + Descuento);
    }
}