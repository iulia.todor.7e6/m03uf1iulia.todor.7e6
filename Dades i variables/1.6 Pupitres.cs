﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 *Descripció: En una escola tenim tres classes i volem saber quin és el nombre de taules que necessitarem
tenir en total. Dependrà del nombre d'alumnes per aula. Cal tenir en compte que a cada taula hi
caben 2 alumnes.**/

internal class Program
{
    private static void Main(string[] args)
    {
        
        Console.WriteLine("Pon tres números y diré cuantos pupitres necesitas");

        //variables
        int num1 = Convert.ToInt32(Console.ReadLine());
        int num2 = Convert.ToInt32(Console.ReadLine());
        int num3 = Convert.ToInt32(Console.ReadLine());

        //cálculo
        int resultat = (num1 + num2 + num3) / 2;
        //resultado
        Console.WriteLine("Necesitarás " + resultat + " pupitres. De nada :)");
    }
}