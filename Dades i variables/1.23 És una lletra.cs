﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Fes un programa que rebi un caràcter i digui si és una lletra o no.**/

internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Introduce un carácter y te diré si es una letra o no");
            
            //variable
            char ascii = Convert.ToChar(Console.ReadLine());

            //la variable se transforma en un íntegro, el cual sería su código ascii. Tiene
            //que estar comprendida entre aquellos carácteres que en la tabla de código ascii
            //van antes de las letras mayúsculas, entre mayúsculas y minúsculas o después de las
            //letras mayúsculas

            bool result = (int)ascii > (int)'@' && (int)ascii < ']' || (int)ascii > (int)'`' && (int)ascii < (int)'{';

            //resultado
            Console.WriteLine(result);
        }
    }

