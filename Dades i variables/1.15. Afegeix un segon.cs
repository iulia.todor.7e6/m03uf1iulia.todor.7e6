﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Fes un programa que afegeixi 1 segon un nombre de segons determinat.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número (en segundos) y sumará uno");
        //variable
        int seg = Convert.ToInt32(Console.ReadLine());
        //el segundo se suma uno
        seg++;
        //de esta forma el segundo se reseteará a 1 si el usuario introduce 60 segundos 
        int num = seg % 60;
        //resultado
        Console.WriteLine(num);



        



    }
}