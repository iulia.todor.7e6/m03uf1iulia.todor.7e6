﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Fes un programa que rebi dos nombres enters i imprimeixi true si el primer és major que el
segon, false en cap altre cas..**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Imprime un número y te diré si el primero es mayor que el segundo");

        //variables
        int i = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Ahora el segundo");
        int j = Convert.ToInt32(Console.ReadLine());
        //si i es mayor que j, entonces es true
        bool resultat = i > j;
        //resultado
        Console.WriteLine(resultat);


    }
}