﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Per poder fer un estudi de la ventilació d'una aula necessitem poder calcular la quantitat d'aire
que hi cap en una habitació. Llegeix les 3 dimensions de l'aula i imprimeix per pantalla quin volum
té.**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Dime las dimensiones de la habitaciones y te diré su volumen");

        //variables
        double al = Convert.ToDouble(Console.ReadLine());
        double am = Convert.ToDouble(Console.ReadLine());
        double ll = Convert.ToDouble(Console.ReadLine());
        //resultado
        Console.WriteLine("El volumen es " + al * am * ll);
    }
}