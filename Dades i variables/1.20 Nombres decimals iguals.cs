﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Fes un programa que rebi dos nombres decimals i torni si són iguals o no**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe dos números decimales y te diré son iguales o no");

        //variables
        float i = Convert.ToInt32(Console.ReadLine());
        float j = Convert.ToInt32(Console.ReadLine());

        //será true si son iguales
        bool valor = i == j;

        //resultado
        Console.WriteLine(valor);



    }
}