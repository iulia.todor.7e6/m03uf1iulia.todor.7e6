﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Fes un programa que rebi un caràcter de l’abecedari en majúscula i el faci minúscula.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe una letra mayúscula y te la paso a minúscula");

        //variable
        char ascii = Convert.ToChar(Console.ReadLine());
        //lo segundo es un espacio, que tiene el 32 en código ASCII. Hay 32 carácteres de diferencia
        //entre mayúsculas y minúsculas
        int conversion = (int)ascii + (int)' ';

        //pasamos el íntegro a carácter con una nueva variable
        char result = (char)conversion;
        
        //resultado
        Console.WriteLine(result);
    }
}