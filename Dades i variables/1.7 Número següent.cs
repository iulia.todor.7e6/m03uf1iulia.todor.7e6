﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 *Descripció: Escriu un programa que llegeixi un nombre enter i imprimeixi una frase amb el següent 
 *nombre enter.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Di un número y te diré el siguiente a este :D");
        
        //variables
        int num1 = Convert.ToInt32(Console.ReadLine());

        //cálculo
        int resultat = num1 + 1;
        //resultado
        Console.WriteLine("El número que va después de " + num1 + " es " + resultat);
    }
}