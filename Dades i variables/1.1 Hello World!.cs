﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Escriu un programa que imprimeixi per pantalla la frase “Hello World!”.**/
internal class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");
    }
}