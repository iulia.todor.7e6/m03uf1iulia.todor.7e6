﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 *Descripció: Una web d'habitatges de lloguer ens ha proposat una ampliació. Volen mostrar l'àrea de les
habitacions per llogar. Fes un programa que ens ajudi a calcular les dimensions d'una habitació.
Llegeix l'amplada i la llargada en metres (enters) i mostra'n l'àrea.**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Recuerdas esa chuleta que hiciste en primero de la ESO? Hora de usarla de nuevo");
        Console.WriteLine("Pon dos números y te diré el área de la habitación");
        Console.WriteLine("Recuerda que el àrea es c*c o b*a, que aquí será lo mismo");

        //variables
        int c1 = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Ahora el segundo número");

        int c2 = Convert.ToInt32(Console.ReadLine());

        //cálculo del área
        int area = c1 * c2;
        //resultado
        Console.WriteLine("El àrea es " + area);
    }
}