﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Escriu un programa que llegeixi un número per entrada i imprimeixi el doble del seu valor.**/

internal class Program
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Escribe un número y te diré su doble :)");

        //variable
        int num = Convert.ToInt32(Console.ReadLine());
        //resultado
        Console.WriteLine(num * 2);
    }
}
