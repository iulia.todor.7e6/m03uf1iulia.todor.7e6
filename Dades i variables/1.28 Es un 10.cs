﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Fes un programa que donats 4 enters ens diguis si hi ha cap que sigui 10**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce 4 números y a ver si alguno es un 10");

        //variables
        int i = Convert.ToInt32(Console.ReadLine());
        int j = Convert.ToInt32(Console.ReadLine());
        int k = Convert.ToInt32(Console.ReadLine());    
        int l = Convert.ToInt32(Console.ReadLine());

        //si alguna de las variables es un diez, el resultado será true
        bool result = i == 10 || j == 10 || k == 10 || l == 10;

        //resultado
        Console.WriteLine(result);


    }
}