﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Fes un programa que calculi l'equivalent en peus una longitud en metres..**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe un número en metros y te lo pasaré a pies");

        //1 metro = 39,37 pulgadas
        //12 pulgadas = 1 pie

        //variable
        double metro = Convert.ToDouble(Console.ReadLine());

        //cálculo
        double pulgada = metro * 39.37;

        double pie = pulgada / 12;

        //resultado
        Console.WriteLine(metro + " metros es igual a " + pie + " pies");


    }
}