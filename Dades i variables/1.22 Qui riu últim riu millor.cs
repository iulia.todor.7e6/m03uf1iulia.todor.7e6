﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Fes un programa que rebi el valor del nivell de riure de 5 persones i comprovi si la dita és certa.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe el poder de risa de 5 personas y a ver si el que rie el último rie mejor");

        //variables
        int i = Convert.ToInt32(Console.ReadLine());
        int j = Convert.ToInt32(Console.ReadLine());
        int k = Convert.ToInt32(Console.ReadLine());
        int l = Convert.ToInt32(Console.ReadLine());
        int m = Convert.ToInt32(Console.ReadLine());

        //m, al ser el último, tiene que ser mayor que todos los demás
        bool result = m > i && m > j && m > k && m > l;

        //resultado
        Console.WriteLine(result);
    }
}