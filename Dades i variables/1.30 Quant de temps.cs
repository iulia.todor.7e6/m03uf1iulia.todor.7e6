﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Fes un programa que calculi quants minuts, hores i segons hi ha en un número de segons
donats.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Pon el número de segundos y te lo traduciré a horas, minutos y segundos");

        //variable de segundos totales
        int total = Convert.ToInt32(Console.ReadLine());

        //hora
        int hora = (total / 60)/60;
        //minutos
        int min = (total - 3600 * hora)/60;
        //segundos
        int seg = total - ((hora * 3600)+(min * 60));

        //resultados
        Console.WriteLine(hora);
        Console.WriteLine(min);
        Console.WriteLine(seg);


    }
}