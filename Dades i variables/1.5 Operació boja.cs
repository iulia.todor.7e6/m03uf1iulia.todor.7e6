﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 *Descripció: L 'usuari escriu 4 enters i s'imprimeix el valor de sumar el primer amb el segon, multiplicat per el
mòdul del tercer amb el quart.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe 4 números y haré MAAAAAAAAAAAGIA");

        //variables
        int num1 = Convert.ToInt32(Console.ReadLine());
        int num2 = Convert.ToInt32(Console.ReadLine());
        int num3 = Convert.ToInt32(Console.ReadLine());
        int num4 = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("NADA POR AQUÍ, NADA POR ALLÀ...");

        Console.WriteLine(".");
        Console.WriteLine(".");
        Console.WriteLine(".");
        Console.WriteLine(".");
        Console.WriteLine(".");
        Console.WriteLine(".");
        Console.WriteLine(".");
        Console.WriteLine(".");
        Console.WriteLine(".");
        Console.WriteLine(".");
        Console.WriteLine(".");
        Console.WriteLine(".");

        //cálculo
        int resultat = (num1 + num2) * (num3 % num4);

        //resultado
        Console.WriteLine("El número que sale es " + resultat);

        Console.WriteLine("Qué he hecho exactamente? No lo sé ni yo, pero serán cinco euros :)");
    }
}