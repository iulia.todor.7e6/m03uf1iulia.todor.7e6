﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 *Descripció: Escriu un programa que llegeixi un nom d’usuari per l’entrada i imprimeixi una frase de
benvinguda.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe tu nombre");
        //variable
        string usuari = Convert.ToString(Console.ReadLine());
        //resultado
        Console.WriteLine("¡Hola, " + usuari + " !");
    }
}