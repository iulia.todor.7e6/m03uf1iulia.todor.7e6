﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Fes un programa que rebi un caràcter de l’abecedari en minúscula i un altre en majúscula i digui
si són corresponents**/


internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un carácter en minúscula");

        //variables
        char minus= Convert.ToChar(Console.ReadLine());

        Console.WriteLine("Ahora uno en mayúscula");

        char mayus= Convert.ToChar(Console.ReadLine());

        //Hay una diferencia de 32 carácteres entre mayúscula y minúsculas en la tabla
        //de código ASCII. Si esta diferencia es correcta, son equivalentes
        bool result = (int)minus - (int)mayus == (int) ' ';        

        //resultado
        Console.WriteLine(result);
    }
}