﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: L'usuari escriu un enter amb la seva edat i s'imprimeix true si és major d'edat, i false en qualsevol
altre cas.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce la edad y te diré si es legal");

        //variables
        int edad = Convert.ToInt32(Console.ReadLine());

        //si edad es menor de 18 significa que no es legal
        bool legal = edad >= 18;
        //resultado
        Console.WriteLine(legal);

    }
}