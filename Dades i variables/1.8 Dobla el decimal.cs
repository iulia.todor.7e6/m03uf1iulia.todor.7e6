﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 *Descripció: Llegeix un valor amb decimals i imprimeix el doble.**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Dame un número decimal y te lo multiplico");

        //variable
        double num = Convert.ToDouble(Console.ReadLine());

        //cálculo
        double resultat = num * 2;
        //resultado
        Console.WriteLine("El resultado es " + resultat);
    }
}