﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Fes un programa que rebi un valor booleà i et retorni el valor contrari**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce true o false y te daré el contrario");

        //variables
        string texto = Convert.ToString(Console.ReadLine());

        bool valor = Convert.ToBoolean(texto);

        //el valor es igual al contrario del valor, así que true será false
        valor = !valor;
        //resultado
        Console.WriteLine(valor);

    }
}