﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Fes un programa que afegeixi donat un nombre enter, imprimeixi el mateix número en decimal.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número entero y te lo imprimiré en decimal");

        //variables
        int num = Convert.ToInt32(Console.ReadLine());

        float numD = (int) num;

        //debería salir el resultado, pero no consigo que salga en decimal
        Console.WriteLine("Ahora te lo convertiré a decimal: " + num);
    }
}