﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Escriu un programa que imprimeixi targetes de treball. Aquestes han de contenir, nom, cognom i
número de despatx de la persona treballadora.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce tu nombre y apellido");

        //variable
        string nombre = Convert.ToString(Console.ReadLine());

        Console.WriteLine("Introduce el número del despacho");

        int despacho = Convert.ToInt32(Console.ReadLine());

        //resultado
        Console.WriteLine("Empleada: " + nombre + " - Despacho: " + despacho);


    }
}