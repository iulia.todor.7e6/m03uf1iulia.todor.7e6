﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Descripció: Feu un programa que rebi una temperatura en graus Celsius i la converteixi en graus Fahrenheit**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Di la tmeperatura y te la convierto a Fahrenheit");

        //variable
        double T= Convert.ToDouble(Console.ReadLine());
        //resultat
        Console.WriteLine("En Fahrenheit son " + ((T * 1.8) + 32) + " grados ");
    }
}