﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Fes un programa que demani un capital, i calculi quin capital es generarà passat cert temps
segons certs interessos (entre 0% i 100%).**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un capital");

        //variables
        double capital = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Introduce durante cuantos años");

        double año = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Introduce el porcentaje de intereses");

        double porcentaje = Convert.ToDouble(Console.ReadLine());

        //cálculo
        double capital_final = (capital) + ((capital * (porcentaje / 100)) * año);
        
        //resultado
        Console.WriteLine("El capital final es " + capital_final);

    }
}