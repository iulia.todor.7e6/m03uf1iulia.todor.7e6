﻿/** Autor: Iulia Todor
 * Data: 06/10/2022
 * Fes un programa que, rebuts dos nombres per l’entrada, comprovi si el primer és divisible pel
segon.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe el divisor");

        //variables
        double divisor = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Ahora el dividendo");

        double dividend = Convert.ToDouble(Console.ReadLine());        
        
        Console.WriteLine("Vamos a ver si estos dos números son divisibles");

        //si el módulo es 0 es que son divisibles
        bool resultat = dividend % divisor == 0;

        //resultado
        Console.WriteLine(resultat);

  


    }
}