﻿/**Autor: Iulia Todor
 * Data: 10 / 10 / 2022
 * L'usuari escriu un valor que representa una nota

Imprimeix "Excelent", "Notable", "Bé", "Suficient", "Suspès", "Nota invàlida" segons  la nota numèrica introduïda.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Pon tu nota");

        int nota = Convert.ToInt32(Console.ReadLine());

        switch (nota)
        {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                    Console.WriteLine("Suspendido");
                    break;
            case 5:
                    Console.WriteLine("Suficiente");
                    break;
            case 6:
                    Console.WriteLine("Bien");
                    break;
            case 7:
            case 8:
                    Console.WriteLine("Notable");
                    break;
            case 9:
            case 10:
                    Console.WriteLine("Excelente");
                break;

            default:
                    Console.WriteLine("Nota inválida");
                    break;
        }

    }
}