﻿/**Autor: Iulia Todor
 * Data: 10 / 10 / 2022
 * L'usuari demana una número de l'1 al 7. Per consola s'imprimerix:  Dilluns, 
 * Dimarts, Dimecres, Dijous,...**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Dime en qué día de la semana estamos (número, no día. Pero número literalmente, no 15 de octubre o algo así");
        
        //variable
        int semana = Convert.ToInt32(Console.ReadLine());

        //dependiendo de qué número introduzca el usuario, aparecerá su correspondiente
        //día de la semana
        switch (semana) { 

            case 1:

                Console.WriteLine("Estamos a lunes");
                break;


            case 2:
        
                Console.WriteLine("Estamos a martes");
                break;

            case 3:

                Console.WriteLine("Estamos a miércoles");
                break;

            case 4: 
                
                Console.WriteLine("Estamos a jueves");
                break;

            case 5:

                Console.WriteLine("Estamos a viernes");
                break;

            case 6:

                Console.WriteLine("Estamos a sábado");
                break;

            case 7:

                Console.WriteLine("Estamos a domingo");
                break;

            default:

                Console.WriteLine("No.");
                break;
        }


    }
}