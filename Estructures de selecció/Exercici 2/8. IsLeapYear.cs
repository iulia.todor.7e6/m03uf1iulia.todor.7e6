﻿/**Autor: Iulia Todor
 * Data: 10 / 10 / 2022
 * Descripció: L'usuari introdueix un any. Indica si és de traspàs printant 
 * "2020 és any de traspàs" o "2021 no és any de traspàs".**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número y te dirá si es de traspaso o no");

        //variable
        double i = Convert.ToDouble(Console.ReadLine());

        //es año de traspaso si es divisible entre 4, no es divisible entre 100 o 
        //es divisible entre 400
        if (i % 4 == 0 && !(i % 100 == 0) || (i % 400 == 0))
        {

            Console.WriteLine("Es un año de traspaso");

        }

        else
        {
            Console.WriteLine("No es un año de traspaso");
        }
    }
}