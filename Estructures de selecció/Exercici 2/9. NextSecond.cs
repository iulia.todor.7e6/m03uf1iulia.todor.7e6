﻿/**Autor: Iulia Todor
 * Data: 10 / 10 / 2022
 * L'usuari introdueix un any. Indica si és de traspàs printant "2020 és any de traspàs" 
 * o "2021 no és any de traspàs".**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce la hora");

        //variable
        int h = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce los minutos");
        int m = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Introduce los segundos");
        int s = Convert.ToInt32(Console.ReadLine());

        //Sumamos +1 segundo
        s += 1;

        //Calculamos qué pasa si los segundos son mayores o iguales a 60
        //Como es un if en vez de else if, los siguientes if tomarán en cuenta
        //el primero
        if (s >= 60)
        {
            s = 0;
            m += 1;
        }
        //Si los minutos son mayores a 60, se resetean a 0 y la hora aumenta en uno
        if (m >= 60)
        {
            m = 0;
            h += 1;
        }
        //Si las horas son mayores o iguales a 24, se resetea a 0
        if (h >= 24)
        {
            h = 0;
        }

        Console.WriteLine("La hora es " + h + ":" + m + ":" + s);
    }
}