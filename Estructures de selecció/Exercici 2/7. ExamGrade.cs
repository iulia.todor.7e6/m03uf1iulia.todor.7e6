﻿/**Autor: Iulia Todor
 * Data: 10 / 10 / 2022
 * Descripció: L 'usuari escriu un valor que representa una nota. Imprimeix "Excelent", 
 * "Notable", "Bé", "Suficient", "Suspès", "Nota invàlida" segons el la nota numèrica 
 * introduïda**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Dime tu nota. Vaaaa, sin miedo ;)");

        //variable
        double nota = Convert.ToDouble(Console.ReadLine());

        //resultado
        if (nota >= 0 && nota < 4.5)
        {
            Console.WriteLine("Has suspendido, pero toma un snickers para compensar " +
                "[====]");
        }

        else if (nota >= 4.5 && nota < 5.5)
        {
            Console.WriteLine("Has aprobado con un suficiente :D");
        }

        else if (nota >= 5.5 && nota < 6.5)
        {
            Console.WriteLine("Has aprobado con un bien :D");
        }

        else if (nota >= 6.5 && nota < 8.5)
        {
            Console.WriteLine("Has aprobado con un notable :D");
        }

        else if (nota >= 8.5 && nota <= 10)
        {
            Console.WriteLine("Has aprobado con un excelente :D");
        }

        else
        {
            Console.WriteLine("¡Llamad a los cazafantasmas!");
        }


    }
}