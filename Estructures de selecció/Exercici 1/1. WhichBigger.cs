﻿using System.Net;

/**Autor: Iulia Todor
 * Data: 10 / 10 / 2022
 * Descripció: 	-Demana dos enters a l'usuari i imprimeix el valor més gran**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce dos números");

        //variables
        double i = Convert.ToDouble(Console.ReadLine());
        double j = Convert.ToDouble(Console.ReadLine());


        //resultat
        if (i < j)
        {
            Console.WriteLine("j es mayor que i");
        }

        else if (j < i)
        {
            Console.WriteLine("i es mayor que j");
        }


        else
        {
            Console.WriteLine("i es igual a j");
        }
    }
}