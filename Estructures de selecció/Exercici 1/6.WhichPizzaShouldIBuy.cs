﻿/**Autor: Iulia Todor
 * Data: 10 / 10 / 2022
 * Descripció: Volem comparar quina pizza és més gran, entre una rectangular i una rodona. 
 * L'usuai entra el diametre d'una pizza rodona
 * L'usuari entra els dos costats de la pizza rectangular
 * Imprimeix "Compra la rodona" si la pizza rodona és més gran, o "Compea la rectangular" 
 * en qualsevol altre cas.**/

internal class Program
{
    private static void Main(string[] args)
    {
        //variables
        Console.WriteLine("Introduce el diàmetro de la pizza redonda");

        double diametro = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("Introduce la base y altura de la pizza cuadrada");

        double b = Convert.ToDouble(Console.ReadLine());
        double a = Convert.ToDouble(Console.ReadLine());

        //cálculos
        double radio = diametro / 2;

        double areadiametro = Math.PI * (radio * radio);
        double arearectangulo = b * a;

        //resultado
        if (areadiametro > arearectangulo)
        {
            Console.WriteLine("Compra la pizza redonda");
        }

        else if (areadiametro < arearectangulo)
        {
            Console.WriteLine("Compra la pizza rectangular");
        }

        else
        {
            Console.WriteLine("Haz lo que te diga el corazón o algo, yo qué sé");
        }

    }
}