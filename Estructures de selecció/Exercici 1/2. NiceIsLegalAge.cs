﻿/**Autor: Iulia Todor
 * Data: 10 / 10 / 2022
 * Descripció: 	Demana l'edat de l'usuari - Printa  "Ets major d'edat", si és major d'edat.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce tu edad");

        //variable
        int edad = Convert.ToInt32(Console.ReadLine());

        //resultado
        if (edad >= 18)
        {
            Console.WriteLine("Eres mayor de edad");
        }

        else
        {
            Console.WriteLine("No eres mayor de edad");
        }


    }
}