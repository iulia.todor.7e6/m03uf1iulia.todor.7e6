﻿/**Autor: Iulia Todor
 * Data: 10 / 10 / 2022
 * Descripció: Introdueix el número de persones i el número de galetes. - Si a tothom li toquen el mateix número de galetes imprimeix "Let's Eat!", sinó imprimeix "Let's Fight"**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el número de personas");

        //variables
        int personas = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Introduce el número de galletas");

        int galletas = Convert.ToInt32(Console.ReadLine());

        //lucharán si hay más galletas que personas o más galletas que personas
        //solo comerán si ambos datos son iguales
        if (personas > galletas || personas < galletas)
        {

            Console.WriteLine("Let's fight");
        
        }

        else
        {
            Console.WriteLine("Let's eat");
        }

    }
}