﻿/**Autor: Iulia Todor
 * Data: 10 / 10 / 2022
 * Descripció: -L'usuari escriu un enter. Imprimeix  "bitllet vàlid"  si existeix un 
 * bitllet d'euros amb la quantitat entrada, "bitllet invàlid" en qualsevol altre cas.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("¿Quieres pagar con billete? Adelante, entrégalo.");

        //variable
        int billete = Convert.ToInt32(Console.ReadLine());

        //solo aceptará billetes existentes
        if (billete == 5  ||
            billete == 10 ||
            billete == 20 || 
            billete == 50 || 
            billete == 100||
            billete == 200||
            billete == 500)
        {
            Console.WriteLine("Graciassss");
        }
        else
        {
            Console.WriteLine("¿Por qué este billete tiene tu cara? ¿Te lo acabas de inventar?");
        }



    }
}