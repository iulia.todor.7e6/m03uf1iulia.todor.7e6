﻿/**Autor: Iulia Todor
 * Data: 10 / 10 / 2022
 * Descripció: Printa el valor absolut d'un enter entrat per l'usuari.**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número entero y te diré su valor absoluto");

        //variable
        int num = Convert.ToInt32(Console.ReadLine());

        //Si el número es negativo este se volverá positivo. Si es mayor que 0 se
        //mantendrá igual
        if (num < 0)
        {
            num = -num;
            Console.WriteLine("El valor de absoluto es: " + num);
        }

        else if (num >= 0)
        {
            Console.WriteLine("El valor de absoluto es: " + num);
        }
    }
}