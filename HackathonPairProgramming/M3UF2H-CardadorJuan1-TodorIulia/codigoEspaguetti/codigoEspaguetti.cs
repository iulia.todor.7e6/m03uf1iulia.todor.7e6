﻿using System;

namespace codigoEspaguetti
{
    class codigoEspaguetti
    {

        public static void Main(string[] args)
        {
            string reiniciar = "";
            do
            {
                       
                Console.Write("\n-----Calculator-----\nBienvenido al menu\n---------------------\n");
                string command;
                int num1 = 0;
                int num2 = 0;
                int resultado = 0;
                Console.WriteLine("Por favor introduzca el comando (suma, resta, multiplica, divide): ");
                command = Console.ReadLine();
                string[] commandSplitted = command.Split(" ");
                try
                {
                    num1 = int.Parse(commandSplitted[1]);
                    num2 = int.Parse(commandSplitted[2]);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error \nDebe añadir valores a operar");
                }
                switch (commandSplitted[0].ToLower())
                {
                    case "suma":
                        resultado = Suma(num1, num2);
                        break;
                    case "resta":
                        resultado = Resta(num1, num2);
                        break;
                    case "multiplica":
                        resultado = Multiplicar(num1, num2);
                        break;
                    case "divide":
                        resultado = Dividir(num1, num2);
                        break;
                    default:
                        Console.WriteLine("Error: Comando incorrecto");
                        break;
                }
                Console.WriteLine("El resultado de la operacion es: " + resultado+"\n------Calculator--------\nPresione la tecla R para reiniciar");
                reiniciar = Console.ReadLine();
            } while (reiniciar == "r");
        }
        public static int Suma(int numero1, int numero2)
        {
            return numero1 + numero2;
        }
        public static int Resta(int numero1, int numero2)
        {
            return numero1 - numero2;
        }
        public static int Multiplicar(int numero1, int numero2)
        {
            return numero1 * numero2;
        }
        public static int Dividir(int numero1, int numero2)
        {
            return numero1 / numero2;
        }
    }
    
}
