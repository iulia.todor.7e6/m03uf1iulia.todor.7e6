﻿using System;
using System.Linq;
namespace ConvertidorTemperaturas
{
    class ConvertidorTemperaturas
    {
        public static void Main(string[] args)
        {
            double grades = 0.0;
            double gradesConverted = 0.0;
            string input;
            string temperaturaEntrada;
            string temperaturaSalida;
            int spaceCounter;
            bool correcto;
          
            //Comprueba que haya 3 parametros
            do
            {
                Console.WriteLine("Introduix els graus, l' unitat de temperatura entrant i l'unitat de temperatura per a la conversio: ");
                correcto = true;
                do
                {
                    spaceCounter = 0;
                    input = Console.ReadLine();
                    for (int i = 0; i < input.Length; i++)
                    {
                        if (input[i] == ' ') spaceCounter++;
                    }
                    if (spaceCounter != 2) Console.WriteLine("Error: has d'introduir tres paraules");
                } while (spaceCounter != 2);
                grades = Convert.ToDouble(input.Split(' ')[0]);
                temperaturaEntrada = input.Split(' ')[1].ToLower();
                temperaturaSalida = input.Split(' ')[2].ToLower();
                gradesConverted = (grades * 1.8) + 32;
                if ( ConversionTemperatura(grades, temperaturaEntrada, temperaturaSalida) == grades) correcto = false;
            } while (correcto == false);
            Console.Write("La temperatura de " + grades + " " + temperaturaEntrada + " es: ");
            Console.WriteLine(ConversionTemperatura(grades, temperaturaEntrada, temperaturaSalida) + " en " + temperaturaSalida);


        }
        public static double ConversionTemperatura(double temperatura, string Entrada, string Salida)
        {
            if (Entrada == "celsius" && Salida == "fahrenheit")
            {
                temperatura = temperatura * 1.8 + 32;
            }
            else if (Entrada == "fahrenheit" && Salida == "celsius")
            {
                temperatura = (temperatura - 32)/1.8;
            }
            else if (Entrada == "fahrenheit" && Salida == "kelvin")
            {
                temperatura = (temperatura - 32) /1.8000 + 273.15;
            }
            else if (Entrada == "kelvin" && Salida == "fahrenheit")
            {
                temperatura = (temperatura - 273.15) *1.8000 + 32.00;
            }
            else if (Entrada == "celsius" && Salida == "kelvin")
            {
                temperatura = temperatura + 273.15;
            }
            else if (Entrada == "kelvin" && Salida == "celsius")
            {
                temperatura = temperatura - 273.15;
            }
            else 
            {
                Console.WriteLine("Error. No has introduit els termes correctes");
            }
            
            return temperatura;
        }
    }
}
