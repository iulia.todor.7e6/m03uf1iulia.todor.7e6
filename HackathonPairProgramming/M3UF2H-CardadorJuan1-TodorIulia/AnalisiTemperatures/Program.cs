﻿using System;
using System.Linq;

namespace AnalisiTemperatures
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;

            do
            {
                Console.WriteLine("Introdueix un comando: BOLCATDADES, MITJANA, MAXIMS, MINIMS, ALTES, BAIXES, ALTES X%, FAHRENHEIT, KELVIN o END per terminar");
                input = Console.ReadLine();

                string[] comandaSegmentada = input.Split(' ');
                double[] dadaNumerica = new double[comandaSegmentada.Length - 1];
                double mitjanaDadaNumerica = 0;

                switch (comandaSegmentada[0])
                {
                    case "BOLCATDADES":
                        for (int i = 1; i < comandaSegmentada.Length; i++)
                        {
                            dadaNumerica[i] = Convert.ToDouble(comandaSegmentada[i]);
                            Console.WriteLine(dadaNumerica[i]);
                        }
                        break;
                    case "MITJANA":
                        mitjanaDadaNumerica = dadaNumerica.Average();
                        Console.WriteLine(mitjanaDadaNumerica);
                        break;
                    case "MAXIMS":
                        Console.WriteLine(dadaNumerica.Max());
                        break;
                    case "MINIMS":
                        Console.WriteLine(dadaNumerica.Min());
                        break;
                    case "ALTES":
                        for (int i = 0; i < dadaNumerica.Length; i++)
                        {
                            if (dadaNumerica[i] > (mitjanaDadaNumerica * (20) / 100)) { Console.WriteLine(dadaNumerica[i]); }
                        }
                        break;
                    case "BAIXES":
                        for (int i = 0; i < dadaNumerica.Length; i++)
                        {
                            if (dadaNumerica[i] < (mitjanaDadaNumerica * (30) / 100)) { Console.WriteLine(dadaNumerica[i]); }
                        }
                        break;
                    case "ALTES X":

                        break;
                    case "FAHRENHEIT":

                        break;
                    case "KELVIN":

                        break;
                    default:
                        Console.WriteLine("Error");
                        break;
                }
            }
            while (input != "END");








        }


        
    }
}
