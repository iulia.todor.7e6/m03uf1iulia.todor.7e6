﻿using System;
using System.IO;
using System.Linq;


namespace ConvertidorTemperaturas
{
    class ConvertidorTemperaturas
    {
        static void Main(string[] args)
        {
            

            var executar = new ConvertidorTemperaturas();
            executar.Menu();



        }
        public void Menu()
        {
            double dineroTotal = 0;
            double ingres;
            double despesa = 0;
            double despesaMajor = despesa;
            bool tancar = false;
            do
            {
                Console.WriteLine(dineroTotal);
                Console.WriteLine
                    (

                    "Introdueix el número de la funció que vols utilitzar: " +
                    "\n\n1. - Afegir un ingrés" +
                    "\n2. - Afegir una despesa" +
                    "\n3. - Estadistiques" +
                    "\n4. - Visualització del compte" +
                    "\n5. - Impostos"
                    );
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\n\nPrem 0 per sortir.\n");
                Console.ForegroundColor = ConsoleColor.White;

                var opcio_funcio = Convert.ToString(Console.ReadKey().KeyChar);
                Console.Clear();

                switch (opcio_funcio)
                {
                    case "0":
                        tancar = true;
                        break;
                    case "1":
                        dineroTotal = afegirIngres(dineroTotal);
                        break;
                    case "2":
                        dineroTotal = afegirDespesa(dineroTotal);
                        if (dineroTotal < 0)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Contabilitat al descobert amb, una descompensació de: " + dineroTotal + "$");
                        }
                        break;
                    case "3":
                        Estadistiques();
                        break;
                    case "4":
                        Cuarta();
                        break;
                    case "5":
                        Quinta();
                        break;
                    default:
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.WriteLine("\nEl valor que has introduït no és vàlid. :(");
                        break;
                }

                if (opcio_funcio != "0")
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("\n\nPrem qualsevol tecla per tornar al menú...");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.ReadKey(true);
                }
                Console.Clear();
            } while (!tancar);
        }
        public double afegirIngres(double dineroTotal)
        {
            double ingres = 0;
            do
            {
                Console.WriteLine("Afegeix l'ingrés");   

                ingres = Convert.ToDouble(Console.ReadLine());
               
                if (ingres < 0)
                {
                    Console.WriteLine("L'ingres no pot ser menor a 0");
                }
            } while (ingres < 0);

            Console.WriteLine("L'ingrés s'ha guardat");
            Console.WriteLine(ingres);

            dineroTotal += ingres;

            return ingres;
           
        }
        public double afegirDespesa(double dineroTotal)
        {
            double despesa = 0;
            do
            {
                Console.WriteLine("Afegeix la despesa");

                despesa = Convert.ToDouble(Console.ReadLine());

                if (despesa < 0)
                {
                    Console.WriteLine("La despesa no pot ser menor a 0");
                }
            } while (despesa < 0);

            

            Console.WriteLine("La despesa s'ha guardat");
            Console.WriteLine(despesa);

            dineroTotal -= despesa;

            return dineroTotal;

        }
        public void Estadistiques()
        {
            Console.WriteLine("3");
            Console.ReadKey();
        }
        public void Cuarta()
        {
            Console.WriteLine("4");
            Console.ReadKey();
        }

        public void Quinta()
        {
            Console.WriteLine("4");
            Console.ReadKey();
        }
    }
}
