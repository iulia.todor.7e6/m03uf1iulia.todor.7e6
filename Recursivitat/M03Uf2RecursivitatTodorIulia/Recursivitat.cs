﻿using System;
using System.ComponentModel.Design;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Security.Cryptography;

namespace Recursivitat
{
    class Recursivitat
    {
        public static void Menu()
        {
            string? opcio;
            do
            {
                Console.WriteLine("0. Sortir");
                Console.WriteLine("1. Factorial");
                Console.WriteLine("2. Doble factorial");
                Console.WriteLine("3. Nombre de dígits");
                Console.WriteLine("4. Nombres creixents");
                Console.WriteLine("5. Reducció de dígits");
                Console.WriteLine("6. Seqüència d'asteriscos");
                Console.WriteLine("7. Primers perfectes");
                Console.WriteLine("8. Torres de Hanoi");

                opcio = Console.ReadLine();

                switch (opcio)
                {
                    case "1":
                        Factorial();
                        break;
                    case "2":
                        dobleFactorial();
                        break;
                    case "3":
                        nombreDigits();
                        break;
                    case "4":
                        nombresCreixents();
                        break;
                    case "5":
                        reduccioDigits();
                        break;
                    case "6":
                        sequenciaAsteriscos();
                        break;
                    case "7":
                        primersPerfectes();
                        break;
                    case "0":
                        Console.WriteLine("Bye");
                        break;
                    default:
                        Console.WriteLine("Opción incorrecta");
                        break;
                }

                Console.ReadLine();

            } while (opcio != "0");
        }
        static void Main()
        {
            Menu();
        }

        /**Autor: Iulia Todor
     * Data: 24/01/2023
     * Descripció: Escriviu una funció que retorni n! Resoleu aquest problema recursivament.**/

        //Función para que el usuario introduzca un íntegro
        static int llegirEnter()
        {
            int enterLlegit;
            enterLlegit = Convert.ToInt32(Console.ReadLine());
            return enterLlegit;
        }

        //Para que el usuario esté obligado a introducir un número positivo
        static int MustBePositive(int PositiveNumber)
        {
            do
            {
                PositiveNumber = llegirEnter();

                if (PositiveNumber <= 0)
                {
                    Console.WriteLine("Debe ser un número positivo o mayor a 0" + "\n");
                }

            } while (PositiveNumber <= 0);

            return PositiveNumber;

        }
        //calcula el factorial del número dado
        public static int calculFactorial(int factorial)
        {
            if (factorial >= 1)
            {
                return factorial * calculFactorial(factorial - 1);
            }

            return 1;
        }


        public static void Factorial()
        {
            int numFactorial = -1;

            Console.WriteLine("Introduce un número y te calcularé su factorial");

            numFactorial = MustBePositive(numFactorial);

            Console.WriteLine(calculFactorial(numFactorial));


        }

        /**Autor: Iulia Todor
         * Data: 24/01/2023
         * Descripció: Escriviu una funció recursiva que retorni n!!. Recordeu que n!! = n × (n − 2) × (n − 4)
         × ...
         Exemple: 9!! = 9 x 7 x 5 x 3 x 1**/

        //calcula el doble factorial de un número dado
        public static int calculDobleFactorial(int factorial)
        {
            if (factorial >= 1)
            {
                return factorial * calculDobleFactorial(factorial - 2);
            }

            return 1;
        }
        public static void dobleFactorial()
        {
            int numFactorial = -1;

            Console.WriteLine("Introduce un número y te calcularé su doble factorial");

            numFactorial = MustBePositive(numFactorial);

            Console.WriteLine(calculDobleFactorial(numFactorial));


        }
        /**Autor: Iulia Todor
     * Data: 24/01/2023
     * Descripció: Escriviu una funció recursiva que retorni el nombre de dígits de n.**/

        //calcula el número de dígitos de un número dado
        public static int calculDigits(int num, int numDigits)
        {
            if (num >= 1)
            {
                numDigits = calculDigits(num / 10, numDigits);
                numDigits++;
            }

            return numDigits;
        }
        public static void nombreDigits()
        {
            int num = -1;
            int numDigits = 0;

            Console.WriteLine("Introduce un número y te calcularé su número de dígitos");

            num = MustBePositive(num);

            Console.WriteLine(calculDigits(num, numDigits));
        }

        /**Autor: Iulia Todor
     * Data: 24/01/2023
     * Descripció: Escriviu una funció recursiva que ens indiqui si un nombre és creixent o no. Un
     nombre és creixent si cada dígit és més petit o igual que el que està a la seva dreta.**/

        //calcula si un número es creciente o no
        public static bool calculCreixent(bool isCreixent, int num)
        {
            int lastDigit = 0;
            int penultimDigit = 0;

            if (num < 10)
            {
                return true;
            }

            else if (num >= 10)
            {
                lastDigit = num % 10;
                penultimDigit = num % 100;
                penultimDigit /= 10;

                if (lastDigit > penultimDigit)
                {
                    return calculCreixent(isCreixent, num / 10);
                }
            }

            return isCreixent;
        }
        public static void nombresCreixents()
        {
            int num = -1;
            bool isCreixent = false;

            Console.WriteLine("Introduce un número y te diré si es creciente o no");

            num = MustBePositive(num);



            Console.WriteLine(calculCreixent(isCreixent, num));
        }

        /**Autor: Iulia Todor
     * Data: 24/01/2023
     * Descripció: Feu una funció recursiva que, donat un natural x, retorni la reducció dels seus dígits.
Direm que reduir els dígits d’un nombre consisteix a calcular la suma dels seus
dígits. Si la suma és un dígit, aquest ja és el resultat. Altrament, es torna a aplicar el
mateix procés a la suma obtinguda, fins a tenir un sol dígit.**/

        //calcula la reducción de los dígitos de un número
        public static int calculReduccioDigits(int num)
        {

            if (num >= 10)
            {
                int lastDigit = num % 10;
                num /= 10;
                num += lastDigit;
                return calculReduccioDigits(num);

            }
            return num;

        }
        public static void reduccioDigits()
        {
            int num = -1;

            Console.WriteLine("Introduce un número y te diré la reducción de sus dígitos");

            num = MustBePositive(num);

            Console.WriteLine(calculReduccioDigits(num));
        }

        /**Autor: Iulia Todor
     * Data: 24/01/2023
     * Descripció: Feu un programa recursiu que llegeixi un natural n i que escrigui 2n − 1 barres amb
       asteriscos seguint el patró que es pot deduir dels exemples.**/
        
        //define el patrón de los asteriscos
        public static int calculAsteriscos(int n)
        {
            if (n > 0)
            {
                calculAsteriscos(n - 1);
                printAsterisco(n);
                Console.WriteLine();
                return calculAsteriscos(n - 1);
            }
            //retorna 1 para poder imprimir un solo asterisco
            return 1;
        }

        //imprime los asteriscos
        public static void printAsterisco(int n)
        {
            for (int i = 0; i < n; i++)
            {
                Console.Write("*");
            }

        }
        public static void sequenciaAsteriscos()
        {
            int num = -1;

            Console.WriteLine("Introduce un número y te imprimiré un patrón de asteriscos súper chulo");

            num = MustBePositive(num);

            calculAsteriscos(num);
        }

        /**Autor: Iulia Todor
    * Data: 24/01/2023
    * Descripció:Escriviu una funció recursiva que ens indiqui si un nombre és primer perfecte o no.
Un primer perfecte ho és si totes les sumes dels seus dígits fins aquesta ser un
nombre d’un dígit donen un nombre primer. Per exemple, 977 és un primer perfecte,
perquè tant 977, com 9 + 7 + 7 = 23, com 2 + 3 = 5, com 5, ..., són tots nombres
primers.**/

        //calcula si el número es primo perfecto 
        public static bool calculPrimers(int num, int notPrimeCounter, bool isPrime, int digitCounter)
        {
            if (num >= 10)
            {
                //restauramos el valor de notPrimeCounter para poder reutilizarlo en checkPrime
                notPrimeCounter = 0;
                int lastDigit = num % 10;
                num /= 10;
                num += lastDigit;
                isPrime = checkPerfectPrimo(num, notPrimeCounter, digitCounter, isPrime);
                return calculPrimers(num, notPrimeCounter, isPrime, digitCounter);
            }

            return isPrime;
        }

        //cuenta el número de dígitos para poder coger luego la reducción.
        public static int countingDigits(int n, int notPrimeCounter, int digitCounter)
        {
            notPrimeCounter = 0;
            digitCounter = 0;
            int container = 0;
            //guarda el valor de n, ya que se hará 0 tras el while
            int n2 = n;
            
            while (n > 0)
            {
                container = n % 10;
                n /= 10;
                digitCounter++;
            }

            return digitCounter;
        }

        //comprueba si la reducción de dígitos también es un número primo
        public static bool checkPerfectPrimo(int n, int notPrimeCounter, int digitCounter, bool isPrime)
        {
            digitCounter = countingDigits(n, notPrimeCounter, digitCounter);

            //para hacer el cálculo solo con la reducción de dígitos
            if (digitCounter == 1)
            {
                isPrime = checkPrimo(n, notPrimeCounter);
            }

            return isPrime;

        }

        //comprueba si el número introducido es primo. También se usará más tarde para definir si la
        //el número es un primo perfecto
        public static bool checkPrimo(int n, int notPrimeCounter)
        {
            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    notPrimeCounter++;
                }

            }

            if (notPrimeCounter != 2)
            {
                return false;
            }

            return true;

        }
        public static void primersPerfectes()
        {
            int num = -1;
            int notPrimeCounter = -1;
            int digitCounter = 0;
            bool isPrime = false;


            Console.WriteLine("Introduce un número");

            num = MustBePositive(num);

            //comprueba que el número introducido sea primo. No he metido el do while dentro de la función
            //checkPrimo para poder reciclarla luego
            do
            {

                notPrimeCounter = 0;

                isPrime = checkPrimo(num, notPrimeCounter);

                if (isPrime == false)
                {
                    Console.WriteLine("Esto no es un número primo");
                    num = MustBePositive(num);
                }

            } while (isPrime == false);

            Console.WriteLine(calculPrimers(num, notPrimeCounter, isPrime, digitCounter));

        }


    }
}

