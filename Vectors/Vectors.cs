﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Numerics;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Vectors
{
    class Vectors
    {
        /**Autor: Iulia Todor
         * Data: 8/11/2022
         * Descripció: Donat un enter, printa el dia de la setmana amb text (dilluns, dimarts, dimecres…), tenint en compte que dilluns es el 0. Els dies de la setmana es guarden en un vector.**/
        public static void DayOfWeek()
        {

            Console.WriteLine("Introduce un numero del 0 al 6 y te dire que dia de la semana le corresponde");
            //Declaramos los dias de la semana
            string[] Dies = new string[7] { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo" };
            //El usuario introduce un numero y la consola devuelve el equivalente en el array
            int num = Convert.ToInt32(Console.ReadLine());

            //Si el numero es mayor o igual a 7
            while (num < 0 || num >= 7)
            {
                Console.WriteLine("Un numero entre 0 y 6, porfa");
                num = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine(Dies[num]);
            Console.WriteLine("Ha sido un placer :)");

            //Pausa para que el menu no salga al instante

            Console.ReadLine();

            Menu();


        }

        /**Autor: Iulia Todor
         * Data: 8/11/2022
         * Descripció: Volem fer un petit programa per guardar l'alineació inicial de jugadors d'un partit de bàsquet. L'usuari introduirà els 5 numeros dels jugadors. Imprimeix-los despres amb el format indicat.**/
        public static void PlayerNumbers()
        {
            Console.WriteLine("Introduce el numero de los 5 jugadores para guardarlos");
            //El limite de jugadores es 5. Creamos un array con los numeros de los dorsales, y este array será de 5 numeros.
            int limit = 5;
            int[] numeros = new int[5];
            //Hacemos un for que acabara cuando la i sea igual al limite. Este irá pidiendo el numero del dorsal. Mientras tanto, dicho dorsal se guardará en la posición del array que corresponda a la i (del 0 al 4)
            for (int i = 0; i < limit; i++)
            {
                numeros[i] = Convert.ToInt32(Console.ReadLine());

                //Para que el usuario no pueda introducir dorsales que no existen
                while (numeros[i] < 1 || numeros[i] > 99)
                {
                    Console.WriteLine("Un numero entre 1 y 99, porfa");
                    numeros[i] = Convert.ToInt32(Console.ReadLine());
                }
            }
            //Este otro for imprimirá los valores de la i en orden, y terminará cuando i sea igual al limite
            for (int i = 0; i < limit; i++)
            {
                Console.Write(numeros[i] + ", ");
            }
        }


        /**Autor: Iulia Todor
         * Data: 8/11/2022
         * Descripció:Volem fer un petit programa per un partit politic. Quan hi ha eleccions, el partit presenta una llista de candidats. Cada candidat te una posició a la llista.
           El programa primer llegirà la llista de candidats (primer introduirem la quantitat i despres un candidat per linia).
           Un cop llegida la llista, l'usuari podrà preguntar quin candidat hi ha a cada posició. El programa acaba quan introdueixi -1. Tingues en compte que els politics no són informàtics, i si indiquen la posició 1, volen dir el primer politic de la llista.
        **/
        public static void CandidatesList()
        {

            Console.WriteLine("Introduce el numero de candidatos");

            //Declaramos las variables
            int num = Convert.ToInt32(Console.ReadLine());
            string[] candidatos = new string[num];
            //Para que el usuario no pueda introducir números negativos o muy altos
            while (num < 1 || num > 10000)
            {
                Console.WriteLine("Un numero válido, porfa");
                num = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Introduce los candidatos");

            //El primer for anyade los números al array, el segundo los imprime
            for (int i = 0; i < num; i++)
            {
                candidatos[i] = Console.ReadLine();
            }
            for (int i = 0; i < num; i++)
            {
                Console.Write(candidatos[i] + ", ");
            }

            Console.WriteLine("Si quieres te puedo decir que candidato hay en cada posicion. Introduce 1 para decir que sí o introduce cualquier otro número para decir que no");
            int posicion = Convert.ToInt32(Console.ReadLine());

            if (posicion == 1)
            {
                Console.WriteLine("Empecemos");
                do
                {   //Para que al imprimir los valores no se tomen como si fuesen de 0 a n, sino de 1 a n
                    Console.WriteLine(candidatos[posicion - 1]);

                    posicion = Convert.ToInt32(Console.ReadLine());

                } while (posicion != -1);

                Console.WriteLine("Ya hemos acabado");
            }

            else
            {
                Console.WriteLine("Adiós.");
            }

        }
        /**Autor: Iulia Todor
        * Data: 8/11/2022
        * Descripció: Donada una paraula i una posicio indica quina lletra hi ha a la posicio indicada
       **/
        public static void LetterInWord()
        {
            Console.WriteLine("Dame una palabra cualquiera");

            //Declaramos la variable
            string palabra = Console.ReadLine();

            Console.WriteLine("Dime la posición de la letra que quieres que te escoja");

            int posicion = Convert.ToInt32(Console.ReadLine());
            //Como el string es una cadena de carácteres, entonces podremos seleccionar la letra que queramos
            Console.WriteLine(palabra[posicion - 1]);

        }
        /**Autor: Iulia Todor
       * Data: 8/11/2022
       * Descripció: Inicialitza un vector de floats de mida 50, amb el valor 0.0f a tots els elements.
Despres assigna els valors següents a les posicions indicades i printa el vector:

      **/
        public static void AddValuesToList()
        {
            float[] num = new float[50];

            num[0] = 31.0f;
            num[1] = 56.0f;
            num[19] = 12.0f;
            //Para imprimir el ultimo (Si fuese solo length significaria que la lista tiene 79 valores
            num[num.Length - 1] = 79.0f;

            for (int i = 0; i < num.Length; i++)
            {
                Console.Write(num[i] + ", ");
            }
        }
        /**Autor: Iulia Todor
      * Data: 8/11/2022
       Descripcio: Donada un vector de 4 números de tipus int,intercanvia el primer per l'últim element.**/

        public static void Swap()
        {
            Console.WriteLine("Introduce 4 numeros enteros e intercambiré la posición del primero por el último");

            //Declaramos el array

            int[] num = new int[4];

            for (int i = 0; i < 4; i++)
            {
                num[i] = Convert.ToInt32(Console.ReadLine());
            }
            for (int i = 0; i < 4; i++)
            {
                Console.Write(num[i] + ", ");
            }

            Console.Write("\n");

            //Intercambiamos el primero por el último

            int contenedor = num[3];
            num[3] = num[0];
            num[0] = contenedor;

            //Volvemos a imprimir el array
            for (int i = 0; i < 4; i++)
            {
                Console.Write(num[i] + ", ");
            }

        }
        /**Autor: Iulia Todor
      * Data: 8/11/2022
      * Descripció: Volem fer un simulador d'un candau com el de la foto:
   La nostra versió, tambe tindrà 8 botons, però el primer serà el 0. A l'inici tots els botons estaran sense premer.
   L'usuari introduirà enters indicant quin botó ha de premer o no.
   Quan introdueix el -1, es que ja ha acabat i hem d'imprimir l'estat del c**/

        public static void PushButtonPadlockSimulator()
        {
            Console.WriteLine("Mira que candado mas chulo");
            Console.WriteLine("\t" + "\t" + "_" + "_" + "_" + "_" + "_");
            Console.WriteLine("\t" + "       " + "/" + "     " + "\\");
            Console.WriteLine("        " + "       " + "|" + "     " + "|");
            Console.WriteLine("        " + "       " + "|" + "     " + "|");
            Console.WriteLine("              " + "-" + "-" + "-" + "-" + "-" + "-" + "-" + "-" + "-");
            Console.WriteLine("      " + "       " + "|" + "         " + "|");
            Console.WriteLine("      " + "       " + "|" + "         " + "|");
            Console.WriteLine("      " + "       " + "|" + "         " + "|");
            Console.WriteLine("      " + "       " + "|" + "         " + "|");
            Console.WriteLine("      " + "       " + "|" + "         " + "|");
            Console.WriteLine("              " + "-" + "-" + "-" + "-" + "-" + "-" + "-" + "-" + "-");

            Console.WriteLine("Tiene numeros del 0 al 7 (Usa tu imaginación, nos falta presupuesto)");
            Console.WriteLine("Quiero que me digas que botones quieres pulsar. Luego te dire al final que botones estan pulsados y cuales no. Pulsa -1 cuando quieras acabar");

            //Declaramos la variable y array
            int pulsar = Convert.ToInt32(Console.ReadLine());
            bool[] estado = new bool[8];

            while (pulsar != -1)
            {
                //El estado cambiará según qué números introduzca
                if (pulsar >= 0 && pulsar <= 7)
                {
                    estado[pulsar] = !estado[pulsar];
                    pulsar = Convert.ToInt32(Console.ReadLine());
                }
                //Si el usuario introduce un número incorrecto
                else
                {
                    Console.WriteLine("Un numero entre 0 y 7 porfa");
                    pulsar = Convert.ToInt32(Console.ReadLine());
                }


            }

            //Imprimimos los números
            for (int i = 0; i < estado.Length; i++)
            {
                Console.WriteLine(estado[i]);
            }

        }
        /**Autor: Iulia Todor
       * Data: 8/11/2022
       * Descripció: Un banc te tot de caixes de seguretat, enumerades del 0 al 10.
         Volem registrar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte.
         L'usuari introduirà enters del 0 al 10 quan s'obri la caixa indicada.
         Quan introduiexi l'enter -1, es que s'ha acabat el dia. Printa per pantalla el nombre de cops que s'ha obert.**/
        public static void BoxesOpenedCounter()
        {
            Console.WriteLine("Tenemos 11 cajas y queremos ver cuantas veces los usuarios las han abierto. Quiero que me digas un numero del 0 al 10. Esto representara que numero de caja" +
            " han abierto los usuarios y contare cuantas veces las han abierto.");

            //declaramos la variable y array
            int abrir = Convert.ToInt32(Console.ReadLine());
            int[] num = new int[11];

            while (abrir != -1)
            {

                if (abrir >= 0 && abrir <= 10)
                {
                    num[abrir]++;
                    abrir = Convert.ToInt32(Console.ReadLine());
                }

                //Si el usuario introduce un número incorrecto
                else
                {
                    Console.WriteLine("He dicho que me des un numero entre 0 y 10. Que se supone que trabajas en un banco, ten algo de sentido comun por dios");
                    abrir = Convert.ToInt32(Console.ReadLine());
                }

            }

            //Imprimimos los valores
            for (int i = 0; i < num.Length; i++)
            {
                Console.Write(num[i] + ", ");
            }
        }
        /**Autor: Iulia Todor
      * Data: 8/11/2022
      * Descripció: L'usuari entra 10 enters. Crea un vector amb aquest valors. Imprimeix per pantalla el valor mes petit introduït.**/
        public static void MinOf10Values()
        {
            Console.WriteLine("Introduce 10 numeros enteros y te dire el mas pequenyo entre estos");

            //Declaramos el array
            int[] num = new int[10];

            for (int i = 0; i < 10; i++)
            {
                num[i] = Convert.ToInt32(Console.ReadLine());
            }

            //Imprime el valor mas pequenyo
            Console.WriteLine(num.Min());

        }
        /**Autor: Iulia Todor
     * Data: 8/11/2022
     * Descripció: Donat el següent vector, imprimeix true si algun dels numeros es divisible entre 7 o false en cas contrari.**/
        public static void IsThereAMultipleOf7()
        {
            //Declaramos el array
            int[] values = new int[37] { 4, 8, 9, 40, 54, 84, 40, 6, 84, 1, 1, 68, 84, 68, 4, 840, 684, 25, 40, 98, 54, 687, 31, 4894, 468, 46, 84687, 894, 40, 846, 1681, 618, 161, 846, 84687, 6, 848 };

            for (int i = 0; i < values.Length; i++)
            {
                //Si es divisible entre 7 imprime true, y si no imprime false
                if (values[i] % 7 == 0)
                {
                    Console.Write(true + ", ");
                }
                else
                {
                    Console.Write(false + ", ");
                }
            }

        }
        /**Autor: Iulia Todor
        * Data: 8/11/2022
        * Descripció: Donat una llista d'enters ordenats de menor a major indica si un cert valor existeix en el bucle.**/
        public static void SearchInOrdered()
        {
            Console.WriteLine("Dime el numero de enteros que quieres introducir");
            //Declaramos variable y array
            int enteros = Convert.ToInt32((Console.ReadLine()));
            int[] nums = new int[enteros];
            Console.WriteLine("Introduce tus numeros");

            for (int i = 0; i < nums.Length; i++)
            {
                enteros = Convert.ToInt32(Console.ReadLine());
            }

     
            int incluido;
            //determina si el numero que ha puesto el usuario esta dentro del array

            bool resultado = false;

            string end = "a";

            //Cuando el usuario introduzca END el bucle terminara
            while (end != "END")
            { 
                Console.WriteLine("Dime un numero y te dire si esta en la lista que has puesto.");

                incluido = Convert.ToInt32(Console.ReadLine());
                //Si el numero está en el array, resultado pasará a ser true
                foreach (int i in nums)
                {
                    if (incluido == nums[i])
                    {
                        resultado = true;
                    }

                }
                Console.WriteLine(resultado);


                Console.WriteLine("Si quieres terminar escribe END. Si quieres continuar pulsa ENTER");

                end = Console.ReadLine();

            }

            Console.WriteLine("Nos vemosss");



        }
        /**Autor: Iulia Todor
    * Data: 8/11/2022
    * Descripció: L'usuari entra 10 enters. Imprimeix-los en l'ordre invers al que els ha entrat.
**/
        public static void InverseOrder()
        {
            Console.WriteLine("Introduce 10 numeros");
            //Declaramos el array
            int[] nums = new int[10];
            //El usuario introduce los 10 numeros
            for (int i = 0; i < nums.Length; i++)
            {
                nums[i] = Convert.ToInt32((Console.ReadLine()));
            }

            //Los imprimimos en orden inverso
            for (int i = 9; i > -1; i--)
            {
                Console.WriteLine(nums[i]);
            }
        }
        /**Autor: Iulia Todor
   * Data: 8/11/2022
   * Descripció: Indica si una paraula és palíndrom.**/
        public static void Palindrome()
        {
            Console.WriteLine("Introduce una palabra y te dire si es palindromo");

            //num será una variable que empieza siendo 0 y aumentará en caso de que haya indicios de que la
            //palabra no es un palindromo. Si para el final num sigue siendo 0, significa que la palabra
            //es un palindromo
            int num = 0;

            string palabra = Console.ReadLine();
            //Para evitar problemas entre mayusculas y mayusculas
            palabra = palabra.ToUpper();


            for (int i = 0; i < palabra.Length; i++)
            {
                Console.Write(palabra[i]);
                if (palabra[i] != palabra[palabra.Length - i - 1])
                {
                    num++;
                }
            }

            if (num == 0)
            {
                Console.WriteLine(true);
            }

            else
            {
                Console.WriteLine(false);
            }



        }
        /**Autor: Iulia Todor
* Data: 8/11/2022
* Descripció: Printa per pantalla ordenats si la llista de N valors introduïts per l'usuari estan ordenats.
L'usuari primer entrarà el número d'enters a introduir i després els diferents enters.**/

        public static void ListSortedValues()
        {

            Console.WriteLine("Dime cuantos numeros quieres");
            //declaramos variable y array
            int num = Convert.ToInt32(Console.ReadLine());
            //Si el usuario trata de introducir un numero negativo
            while (num < 0)
            {
                Console.WriteLine("Un numero positivo, porfa");
                num = Convert.ToInt32(Console.ReadLine());
            }


            Console.WriteLine("Ahora introduce esos numeros");

            int[] list = new int[num];

            //El usuario introduce los numeros y los guardamos en el array
            for (int i = 0; i < num; i++)
            {
                list[i] = Convert.ToInt32(Console.ReadLine());
            }

            //ordenado es un int que empieza siendo 0 y ayudara a saber si la lista está ordenada o no
            int ordenado = 0;

            //Si i es mayor a j, significa que el elemento en la lista es mayor que el anterior. Si para el final la
            //variable ordenado sigue siendo 0, significa que toda la lista esta ordenada
            for (int i = 1; i < list.Length; i += 2)
            {
                for (int j = 0; j < i; j += 2)
                {
                    if (list[i] < list[j])
                    {
                        ordenado++;
                    }
                }
            }
            if (ordenado > 0)
            {
                Console.WriteLine("desordenado");
            }

            else
            {
                Console.WriteLine("ordenado");
            }



        }
        /**Autor: Iulia Todor
* Data: 8/11/2022
* Descripció: Printa per pantalla cap i cua si la llista de N valors introduïts per l'usuari són cap i cua (llegits en ordre invers és la mateixa llista).
**/
        public static void CapICuaValues()
        {
            Console.WriteLine("Dime cuantos numeros quieres");
            //Declaramos variable y array
            int num = Convert.ToInt32(Console.ReadLine());
            //Si el usuario trata de introducir un numero negativo
            while (num < 0)
            {
                Console.WriteLine("Un numero positivo, porfa");
                num = Convert.ToInt32(Console.ReadLine());
            }


            Console.WriteLine("Introduce los numeros y te dire si son capicua o no");

            int[] list = new int[num];

            for (int i = 0; i < num; i++)
            {
                list[i] = Convert.ToInt32(Console.ReadLine());

                //Si el usuario trata de introducir un numero negativo
                while (list[i] < 0)
                {
                    Console.WriteLine("Un numero positivo, porfa");
                    list[i] = Convert.ToInt32(Console.ReadLine());
                }
                
            }

            //Value ayudará a saber si el numero es capicua o no. Aumenta si hay indicios de que el numero no es capicua.
            //Si para el final de la rotación value sigue siendo 0, significa que el numero es capicua

            int value = 0;

            //Poniendo de ejemplo el numero 27672
            //Primera rotación: el numero en posicion 0 seria 2. Length seria 5. El numero en posicion 4 (5 -0 -1) tambien seria 2
            //Segunda rotación: el numero en posicion 1 seria 7. El numero en posicion 3 (5 -1 -1) tambien seria 7
            //Tercera sotacion: el numero en posicion 2 seria 6. El numero en posición 2 (5 -2 -1) tambien seria 6
            //Cuarta rotación: el numero en posición 3 seria 7. El numero en posicion 1 (5 -3 -1) tambien seria 7.
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i] != list[list.Length - i - 1])
                {
                    value++;
                }
            }
            if (value == 0)
            {
                Console.WriteLine("Es capicua");
            }

            else
            {
                Console.WriteLine("No es capicua");
            }


        }
        /**Autor: Iulia Todor
* Data: 8/11/2022
* Descripció: L'usuari introduirà 2 vectors de valors, primer mida després introdueix elements.
Printa per pantalla són iguals si ha introduït el mateix vector, o no són iguals si són diferents.

**/
        public static void ListSameValues()
        {
            Console.WriteLine("Introduce de que tamanyo quieres el vector");

            int tamanyo = Convert.ToInt32(Console.ReadLine());
            //Si el usuario trata de introducir un numero negativo
            while (tamanyo < 0)
            {
                Console.WriteLine("Un numero positivo, porfa");
                tamanyo = Convert.ToInt32(Console.ReadLine());
            }

            //Declaramos dos arrays del mismo tamanyo, el cual es el que el usuario ha escogido
            int[] list1 = new int[tamanyo];
            int[] list2 = new int[tamanyo];

            Console.WriteLine("Introduce los numeros del primer vector");

            for (int i = 0; i < tamanyo; i++)
            {
                list1[i] = Convert.ToInt32(Console.ReadLine());

                //Si el usuario trata de introducir un numero negativo
                while (list1[i] < 0)
                {
                    Console.WriteLine("Un numero positivo, porfa");
                    list1[i] = Convert.ToInt32(Console.ReadLine());
                }
            }

            Console.WriteLine("Introduce los numeros del segundo vector");

            for (int i = 0; i < tamanyo; i++)
            {
                list2[i] = Convert.ToInt32(Console.ReadLine());
                //Si el usuario trata de introducir un numero negativo
                while (list2[i] < 0)
                {
                    Console.WriteLine("Un numero positivo, porfa");
                    list2[i] = Convert.ToInt32(Console.ReadLine());
                }
            }

            int value = 0;

            //Si alguno de los elementos de una lista es diferente de su respectiva contraparte
            //en la otra lista, value aumenta. Si para el final value es mayor a 0, es que las listas no son iguales
            for (int i = 0; i < tamanyo; i++)
            {
                if (list1[i] != list2[i])
                {
                    value++;
                }
            }

            if (value > 0)
            {
                Console.WriteLine("Son diferentes");
            }

            else
            {
                Console.WriteLine("Son iguales");
            }

        }
        /**Autor: Iulia Todor
* Data: 8/11/2022
* Descripció: L'usuari introdueix una llista de valors.
Imprimeix per pantalla la suma d'aquests valors.
**/
        public static void ListSumValues()
        {
            Console.WriteLine("Introduce de que tamanyo quieres el vector");

            //Declaramos la variable y array

            int tamanyo = Convert.ToInt32(Console.ReadLine());
            //Si el usuario trata de introducir un numero negativo
            while (tamanyo < 0)
            {
                Console.WriteLine("Un numero positivo, porfa");
                tamanyo = Convert.ToInt32(Console.ReadLine());
            }


            int[] list = new int[tamanyo];


            Console.WriteLine("Introduce los numeros del vector");

            for (int i = 0; i < tamanyo; i++)
            {
                list[i] = Convert.ToInt32(Console.ReadLine());

                //Si el usuario trata de introducir un numero negativo
                while (list[i] < 0)
                {
                    Console.WriteLine("Un numero positivo, porfa");
                    list[i] = Convert.ToInt32(Console.ReadLine());
                }
            }

            //result empieza como 0 y se le van sumando los componentes del array
            int result = 0;

            for (int i = 0; i < tamanyo; i++)
            {
                result += list[i];
            }

            Console.WriteLine("El resultado es: " + result);
        }
        /**Autor: Iulia Todor
* Data: 8/11/2022
* Descripció: En una botiga volem convertir tot de preus sense a IVA al preu amb IVA. Per afegir l'IVA a un preu hem de sumar-hi el 21% del seu valor.
L'usuari introduirà el preu de 10 articles. Imprimeix per pantalla el preu amb l'IVA afegit amb el següent format indicat a continuació. El programa no pot imprimir res fins a que hagi llegit tots els valors.

**/
        public static void IvaPrices()
        {
            Console.WriteLine("Introduce el precio de 10 articulos");

            //Declaramos el array
            int[] list = new int[10];

            for (int i = 0; i < 9; i++)
            {
                list[i] = Convert.ToInt32(Console.ReadLine());

                //Si el usuario trata de introducir un numero negativo
                while (list[i] < 0)
                {
                    Console.WriteLine("Un numero positivo, porfa");
                    list[i] = Convert.ToInt32(Console.ReadLine());
                }
            }


            //result es el double que se multiplica por los componentes del array, dando el precio con IVA
            double result = 0;

            for (int i = 0; i < 9; i++)
            {
                result = list[i] * 0.21;

                for (int j = i; j <= i; j++)
                {
                    Console.WriteLine(list[i] + "IVA = " + result);
                }
            }
        }
        /**Autor: Iulia Todor
* Data: 8/11/2022
* Descripció: El departament de salut ens ha demanat que calculem la taxa d’infecció que està tenint la Covid en la nostre regió sanitària. Donat un nombre de casos 
casos1
casos1 en una setmana, si la següent tenim un nombre de casos 
casos2
casos2, podem calcular la taxa d'infecció amb la fórmula
infecció =  casos2/casos1
L'usuari introduirà un llistat de casos detectats cada setmana. Imprimeix la taxa d'infecció detectada cada setmana. El programa no pot imprimir res fins a que hagi llegit tots els valors.

**/
        public static void CovidGrowRate()
        {
            Console.WriteLine("Introduce el numero de casos");


            //Declaramos la variable y array
            int num = Convert.ToInt32(Console.ReadLine());
            //Si el usuario trata de introducir un numero negativo
            while (num < 0)
            {
                Console.WriteLine("Un numero positivo, porfa");
                num = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Ahora introduce los casos");

            int[] list = new int[num];

            for (int i = 0; i < num; i++)
            {
                list[i] = Convert.ToInt32(Console.ReadLine());
                //Si el usuario trata de introducir un numero negativo
                while (list[i] < 0)
                {
                    Console.WriteLine("Un numero positivo, porfa");
                    list[i] = Convert.ToInt32(Console.ReadLine());
                }
            }

            int valor = 0;
            //valor empieza como 0. Se convertirá en la división de i y j, luego se imprimirá

            for (int i = 1; i <= num; i++)
            {
                for (int j = i - 1; j < i; j++)
                {
                    valor = list[i] / list[j];
                }

                for (int k = i; k <= i; k++)
                {
                    Console.WriteLine(valor + ", ");
                }
            }
        }
        /**Autor: Iulia Todor
* Data: 8/11/2022
* Descripció: Donada una velocitat d'una bicicleta en metres per segon, indica els metres que haurà recorregut quan hagi passat 1,2,3,4,5,6,7,8,9 i 10 segons.

**/
        public static void BicicleDistance()
        {
            Console.WriteLine("Introduce la velocidad");

            double velocidad = Convert.ToDouble(Console.ReadLine());

            //Si el usuario trata de introducir un numero negativo
            while (velocidad < 0)
            {
                Console.WriteLine("Un numero positivo, porfa");
                velocidad = Convert.ToDouble(Console.ReadLine());
            }

            //Definimos el array
            double[] list = new double[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            //Valor empieza como 0 y se convierte en la multiplicación entre los valores de lista y la velocidad
            double valor = 0;

            for (int i = 0; i < list.Length; i++)
            {
                valor = list[i] * velocidad;

                for (double j = i; j <= i; j++)
                {
                    Console.WriteLine("El valor de la velocidad es: " + valor);
                }
            }

        }
        /**Autor: Iulia Todor
* Data: 8/11/2022
* Descripció: L'usuari introdueix una llista de valors. Imprimeix per pantalla el valor que està més proper a la mitjana dels valors de la llista (calcula la mitjana dels valors primer i cerca el més proper després).
**/
        public static void ValueNearAvg()
        {
            Console.WriteLine("Introduce cuantos numeros quieres");

            int num = Convert.ToInt32(Console.ReadLine());
            //Si el usuario trata de introducir un numero negativo
            while (num < 0)
            {
                Console.WriteLine("Un numero positivo, porfa");
                num = Convert.ToInt32(Console.ReadLine());
            }



            Console.WriteLine("Ahora introduce los numeros");

            int[] list = new int[num];

            //mayor sera el numero mas grande de la lista. Media sera la media entre los numeros
            int mayor = 0;
            int media = 0;

            for (int i = 0; i < num; i++)
            {
                list[i] = Convert.ToInt32(Console.ReadLine());

                //Si el usuario trata de introducir un numero negativo
                while (list[i] < 0)
                {
                    Console.WriteLine("Un numero positivo, porfa");
                    list[i] = Convert.ToInt32(Console.ReadLine());
                }

                //Primero la media es la suma de todos los numeros
                media += list[i];

                //Para tomar el valor mas grande de la lista
                if (list[i] > mayor)
                {
                    mayor = list[i];
                }
            }

            //Ahora media sí es la media de los numeros
            media /= num;

            //la variable maenordistancia tiene el valor del numero mas grande de la lista
            //si la distancia es menor a la mayor distancia, entonces esta pasara a ser la menor distancia
            double menordistancia = mayor;
            //cuando tengamos la distancia mas cercana a la media, mascerca tomara el valor de este
            //elemento del array
            double mascerca = 0;

            for (int i = 0; i < num; i++)
            {
                //vamos calculando la distancia a la media de todos los valores de i hasta obtener el menor posible
                double distancia = list[i] - media;
                //para vigilar los numeros negativos
                distancia = Math.Abs(distancia);
                if (distancia < menordistancia)
                {
                    menordistancia = distancia;
                    //mas cerca sera el elemento de la lista con menor distancia a la media
                    mascerca = list[i];

                }
            }
            Console.WriteLine("Mas cercano: " + mascerca);

        }
        /**Autor: Iulia Todor
* Data: 8/11/2022
* Descripció: Es vol fer un programa per verificar que un codi ISBN (International Standard Book Code) és correcte. El format d’un codi ISBN és: Codi de grup (1 dígit), Codi de l’editor (4 dígits), Codi del llibre (4 dígits), Caràcter/dígit de control (1 caràcter/dígit)
**/
        public static void ISBN()
        {
            Console.WriteLine("Introduce los 10 numeros del ISBN");

            //Declaramos el array
            int[] ISBN = new int[10];

            for (int i = 0; i < ISBN.Length; i++)
            {
                ISBN[i] = Convert.ToInt32(Console.ReadLine());
                //Si el usuario trata de introducir un numero negativo
                while (ISBN[i] < 0)
                {
                    Console.WriteLine("Un numero positivo, porfa");
                    ISBN[i] = Convert.ToInt32(Console.ReadLine());
                }
            }
            
            int sumador = 0;
            //El sumador se encarga de multiplicar los valores del array por la posición de i y luego sumarlos

            for (int i = 0; i < ISBN.Length - 1; i++)
            {
                sumador += ISBN[i] * (i + 1);
                Console.WriteLine(sumador);
            }

            Console.WriteLine("Sumador: " + sumador);
            Console.WriteLine("Modul: " + (sumador % 11));

            //Si el modul de sumador i 11 es igual al codi de control o a 10, l'ISBN es valid 
            if (sumador % 11 == ISBN[9] || sumador % 11 == 10)
            {
                Console.WriteLine(true);
            }

            else
            {
                Console.WriteLine(false);
            }

        }
        /**Autor: Iulia Todor
* Data: 8/11/2022
* Descripció: Volem verificar si un codi ISBN és correcte. Mitjancant el dígit de control que és el dígit número 13. Es calcula de la manera següent:
.
**/
        public static void ISBN13()
        {
            Console.WriteLine("Introduce los 13 numeros del ISBN");

            //Declaramos el array
            int[] ISBN13 = new int[13];

            for (int i = 0; i < ISBN13.Length; i++)
            {
                ISBN13[i] = Convert.ToInt32(Console.ReadLine());
                //Si el usuario trata de introducir un numero negativo
                while (ISBN13[i] < 0)
                {
                    Console.WriteLine("Un numero positivo, porfa");
                    ISBN13[i] = Convert.ToInt32(Console.ReadLine());
                }
            }

            int sumador = 0;
            //El sumador será la suma de los valores del ISBN13

            for (int i = 0; i < ISBN13.Length - 1; i++)
            {
                //Las posiciones pares se multiplicaran por 1 y las impares por 3
                if (i % 2 == 0)
                {
                    ISBN13[i] *= 1;
                }

                else
                {
                    ISBN13[i] *= 3;
                }
                sumador += ISBN13[i];
            }

            Console.WriteLine("Sumador: " + sumador);

            Console.WriteLine("Modul: " + sumador % 10);

            //en caso de que la suma de los 12 primeros digitos no sea multiplo de 10, se le suma la posicion del caracter de control
            if (sumador % 10 != 0)
            {
                sumador += ISBN13[12];



                if (sumador % 10 == 0)
                {
                    Console.WriteLine("valid");
                }

                else
                {
                    Console.WriteLine("No valid");
                }
            }

            //En caso de que la suma ya sea un multiplo de 10
            else
            {
                if (ISBN13[12] != 0)
                {
                    Console.WriteLine("No valid");
                }

                else
                {
                    Console.WriteLine("Valid");
                }
            }









        }

        public static void Menu()
        {
            string opcio;
            do
            {
                Console.WriteLine("0. Sortir");
                Console.WriteLine("1. DayOfWeek");
                Console.WriteLine("2. PlayerNumbers");
                Console.WriteLine("3. CandidatesList");
                Console.WriteLine("4. LetterInWord");
                Console.WriteLine("5. AddValuesToList");
                Console.WriteLine("6. Swap");
                Console.WriteLine("7. PushButtonPadlockSimulator");
                Console.WriteLine("8. BoxesOpenedCounter");
                Console.WriteLine("9. MinOf10Values");
                Console.WriteLine("a. IsThereAMultipleOf7");
                Console.WriteLine("b. SearchInOrdered");
                Console.WriteLine("c. InverseOrder");
                Console.WriteLine("d. Palindrome");
                Console.WriteLine("e. ListSortedValues");
                Console.WriteLine("f. CapICuaValors");
                Console.WriteLine("g. ListSameValues");
                Console.WriteLine("h. ListSumValues");
                Console.WriteLine("i. IvaPrices");
                Console.WriteLine("j. CovidGrowRates");
                Console.WriteLine("k. BicicleDistance");
                Console.WriteLine("l. ValueNearAvg");
                Console.WriteLine("m. ISBN");
                Console.WriteLine("n. ISBN13");


                opcio = Console.ReadLine();

                switch (opcio)
                {
                    case "1":
                        DayOfWeek();
                        break;
                    case "2":
                        PlayerNumbers();
                        break;
                    case "3":
                        CandidatesList();
                        break;
                    case "4":
                        LetterInWord();
                        break;
                    case "5":
                        AddValuesToList();
                        break;
                    case "6":
                        Swap();
                        break;
                    case "7":
                        PushButtonPadlockSimulator();
                        break;
                    case "8":
                        BoxesOpenedCounter();
                        break;
                    case "9":
                        MinOf10Values();
                        break;
                    case "a":
                        IsThereAMultipleOf7();
                        break;
                    case "b":
                        SearchInOrdered();
                        break;
                    case "c":
                        InverseOrder();
                        break;
                    case "d":
                        Palindrome();
                        break;
                    case "e":
                        ListSortedValues();
                        break;
                    case "f":
                        CapICuaValues();
                        break;
                    case "g":
                        ListSameValues();
                        break;
                    case "h":
                        ListSumValues();
                        break;
                    case "i":
                        IvaPrices();
                        break;
                    case "j":
                        CovidGrowRate();
                        break;
                    case "k":
                        BicicleDistance();
                        break;
                    case "l":
                        ValueNearAvg();
                        break;
                    case "m":
                        ISBN();
                        break;
                    case "n":
                        ISBN13();
                        break;
                    case "0":
                        Console.WriteLine("Bye");
                        break;
                    default:
                        Console.WriteLine("Opción incorrecta");
                        break;
                }

                Console.ReadLine();

            } while (opcio != "0");
        }
        static void Main()
        {
            Menu();
        }

    }
}
