﻿/**Autor: Iulia Todor
 * Data: 12/12/2022
 * Descripció: Projecte de la UF1 de M03. Un joc de Wordle a la terminal**/

internal class Wordle
{
    public static void Intro()
    {
        //Es la primera pantalla, donde le preguntamos al usuario si quiere leer las reglas. Luego empieza el juego
        string? opcio;

        do
        {
            Console.WriteLine("¡Hola! Vamos a jugar a un Wordle de Pokémon." + "\n");

            Console.WriteLine("Antes de empezar, dime: ¿te gustaría leer las reglas?" + "\n");

            Console.WriteLine("Escribe Y para leer las reglas.");
            Console.WriteLine("Escribe N para empezar a jugar.");
            Console.WriteLine("Escribe S para salir" + "\n");

            opcio = Console.ReadLine()?.ToUpper();



            switch (opcio)
            {
                case "Y":
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Por si nunca has jugado a un Wordle, estas son las reglas: " + "\n");
                    Console.WriteLine("Tenemos una palabra oculta. Esta es el nombre de un Pokémon de cinco de letras. Tu objetivo es adivinar esta palabra oculta; para hacerlo tienes que escribir palabras de cinco letras y te diré las letras que hayas acertado." + "\n");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("-Si se colorea en verde es que la letra está en la posición correcta.");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("-Si se colorea en amarillo es que la letra está en la palabra, pero en la posición incorrecta.");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("-Si se colorea en rojo es que la letra no está en la palabra." + "\n");
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Tienes 6 intentos para adivinar la palabra. Puedes usar cualquier palabra para tratar de adivinar al Pokémon, lo importante es que lo hagas antes de que se te acaben los intentos. También ten en cuenta que la palabra oculta no tiene ninguna letra repetida" + "\n");
                    Console.WriteLine("Dicho esto te dejaré jugar, espero que lo pases muy bien :)" + "\n");
                    Console.ResetColor();
                    Game();
                    break;
                case "N":
                    Game();
                    break;
                case "S":
                    Console.WriteLine("Adiós");
                    break;
                default:
                    Console.WriteLine("Opción incorrecta. Pulsa enter para continuar");
                    break;
            }

            Console.ReadLine();

        } while (opcio != "S");
    }
    public static void Game()
    {

        Console.WriteLine("Muy bien, empecemos. Escribe tu palabra" + "\n");

        //El array con la lista de Pokémon
        string[] pokemonList = {"Absol", "Arbok", "Azelf", "Bagon", "Burmy", "Deino", "Ekans",
        "Gible", "Hypno", "Inkay", "Lokix", "Lotad", "Lugia", "Luxio", "Magby", "Nacli", "Numel", "Pawmi",
        "Pawmo", "Pichu", "Ralts", "Riolu", "Shinx", "Snivy", "Tepig", "Zorua", "Zubat"};

        //Generamos una palabra aleatoria entre el array
        Random random = new Random();
        int randomNumber = random.Next(pokemonList.Length);
        string randomWord = pokemonList[randomNumber];
        //Transformamos la palabra a mayúscula para evitar problemas entre mayúsculas y minúsculas
        randomWord = randomWord.ToUpper();

        //He dejado esta línea comentada. Es para que aparezca la palabra random antes de empezar. La estaba
        //usando cuando hacía pruebas, pero si la necesitas aquí está
        //Console.WriteLine(randomWord);


        //La palabra que introduce el usuario
        string? guess;
        //El número de intentos
        int attempts = 6;
        //Variable usada para la posición de las letras en las palabras
        int position = 0;

        do
        {
            //Transformamos la palabra a mayúscula para evitar problemas entre mayúsculas y minúsculas
            guess = Console.ReadLine()?.ToUpper();

            //Por si el usuario introduce una palabra más larga o más corta de lo pedido
            while (guess?.Length != 5)
            {
                Console.WriteLine("Recuerda que la palabra debe ser de 5 letras" + "\n");
                guess = Console.ReadLine()?.ToUpper();
            }



            //Empezamos un for. Este acabará cuando termine de analizar cada letra
            for (int i = 0; i < guess.Length; i++)
            {
                //Por si la letra está en la posición correcta
                if (randomWord[i] == guess[i])
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write(guess[i]);
                    Console.ResetColor();
                }

                //Si la letra no está en la posición correcta entramos en dos casos: si la letra no está en la palabra
                //y si está, pero no en la posición correcta
                else if (randomWord[i] != guess[i])
                {
                    //Si la letra de la palabra que ha introducido el usuario se encuentra en la palabra oculta, position aumenta
                    for (int j = 0; j < guess.Length; j++)
                    {
                        if (randomWord[j] == guess[i])
                        {position++;}

                    }
                    //Si position es mayor que 0 significa que la letra está en la palabra oculta, pero no en la posición correcta.
                    if (position != 0)
                    {
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(guess[i]);
                        Console.ResetColor();
                        //Hacemos que position vuelva a ser 0 para que en el siguiente ciclo no se acumule su valor
                        position = 0;
                    }

                    //Si position es igual a 0 significa que la letra no está en la palabra
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write(guess[i]);
                        Console.ResetColor();
                    }
                }

            }
            //Si la palabra es igual a la oculta
            if (randomWord == guess)
            {
                Console.WriteLine();
                Console.WriteLine("Has ganado!" + "\n");
                break;
            }
            //Si la palabra no es la oculta, los intentos disminuyen 
            else
            {
                attempts--;
                Console.WriteLine();
                Console.WriteLine("Intentos restantes: " + attempts + "\n");
            }

        } while (attempts != 0);

        //Si el usuario se ha quedado sin intentos aparece esto
        if (attempts == 0)
        {Console.WriteLine("¡Has perdido! La respuesta correcta era: " + randomWord + "\n");}

        Console.WriteLine("Si quieres volver a jugar pulsa Enter");

    }

    static void Main()
    {
        Intro();
    }




}
