﻿using System.Collections.Generic;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;

namespace Ficheros
{
    internal class Ficheros
    {
        private static void Main(string[] args)
        {
            Menu();

        }

        public static void Menu()
        {
            string path = Path.GetFullPath(@"C:\Users\Colli\source\repos\");
            //string path = Path.GetFullPath(@"..\..\..");
            string fullPathFiles = path + @"M03Uf3FitxersTodorIulia\";
            Directory.CreateDirectory(fullPathFiles);

            string opcio;
            do
            {
                Console.WriteLine("0. Sortir");
                Console.WriteLine("1. Parell i senar");
                Console.WriteLine("2. Inventario");
                Console.WriteLine("3. Word Count");
                Console.WriteLine("4. Config File Language");
                Console.WriteLine("5. Find Paths");

                opcio = Console.ReadLine();

                switch (opcio)
                {
                    case "1":
                        ParellSenar(fullPathFiles);
                        break;
                    case "2":
                        Inventari(fullPathFiles);
                        break;
                    case "3":
                        wordCount(fullPathFiles);
                        break;
                    case "4":
                        configFileLanguage(fullPathFiles);
                        break;
                    case "5":
                        FindPaths();
                        break;
                    case "0":
                        Console.WriteLine("Bye");
                        break;
                    default:
                        Console.WriteLine("Opción incorrecta");
                        break;
                }

                Console.ReadLine();

            } while (opcio != "0");
        }
        public static string ParellSenar(string fullPathFiles)
        {
            /**Crear un método llamado ParellSenar que devuelve "parell.txt" o "sencer.txt" y recibe
            la ruta del directorio donde se encuentra el fichero “Numbers.txt”
            Y un método IsParell que recibe el dato leído y devuelve true si es un número par y
            false si es impar.**/

            string result = "";
            string fileToRead = "Numbers.txt";
            string filePath = fullPathFiles + fileToRead;
            List<string> content = new List<string>();
            if (File.Exists(filePath))
            {
                string s = Path.GetFullPath(fullPathFiles);

                //Leer el fichero origen Number.txt.
                StreamReader sr = File.OpenText(filePath); //ABRIR FICHERO
                while ((sr.ReadLine()) != null)
                {
                    content.Add(sr.ReadLine());
                }
                sr.Close(); // CERRAR EL FICHERO
                content.Reverse();
                for (int i = 0; i < content.Count; i++)
                {
                    if (isParell(content[i]))
                    {
                        result = "parell.txt";
                        WriteNumberInFile(fullPathFiles, result, content[i]);
                    }
                    else if (!isParell(content[i]))
                    {
                        result = "sencer.txt";
                        WriteNumberInFile(fullPathFiles, result, content[i]);
                    }
                }

                Console.WriteLine("Los números han sido imprimidos con éxito");
            }
            else
            {
                Console.WriteLine($"No existe el fichero en este directorio");
            }
            return result;

        }

        private static void WriteNumberInFile(string path, string fileName, string line)
        {


            StreamWriter file;
            //CREAR ARCHIVO SI NO EXISTE
            if (!File.Exists(fileName))
            {
                file = new StreamWriter(fileName);
            }
            else
            {
                file = File.AppendText(fileName); //APPEND sirve Añadir nueva línea al final del fichero, sin machacar lineas anteriores
            }

            file.WriteLine(line);
            file.Close();
        }

        private static bool isParell(string line)
        {
            bool result;
            //ALGORITMO PARA VERIFICAR SI UN NUMERO ES PAR
            int value = Convert.ToInt16(line);
            if ((value % 2) == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return result;
        }

        public static string Inventari(string fullPathFiles)
        { /** Crea un programa para gestionar un inventario, hazlo persistente guardando los datos
            en un fichero txt. Separa los campos utilizando una coma.
            Cuando se añade un nuevo elemento al fichero, los registrados anteriormente o en otra
            sesión no deben desaparecer. Puedes ayudarte del método File.AppendText(filePath)
            Permite que el usuario pueda listar los elementos del inventario.**/

            string result = "";
            string fileToRead = "fichero.txt";
            string filePath = fullPathFiles + fileToRead;
            List<string> content = new List<string>();
            if (File.Exists(filePath))
            {
                Console.WriteLine("Introduce el nombre: ");
                content.Add(Console.ReadLine());
                Console.WriteLine("Introduce el modulo: ");
                content.Add(Console.ReadLine());
                Console.WriteLine("Introduce el MAC: ");
                content.Add(Console.ReadLine());
                Console.WriteLine("Introduce el año de fabriación: ");
                content.Add(Console.ReadLine());

                using (StreamWriter sw = File.AppendText(filePath))
                {

                    for (int i = 0; i < content.Count; i++)
                    {
                        sw.Write(content[i] + ", ");
                    }
                    sw.WriteLine();
                }

                Console.WriteLine("Los elementos han sido introducidos en el fichero con éxito");

            }

            else
            {
                Console.WriteLine($"No existe el fichero en este directorio");
            }

            return result;
        }

        public static string wordCount(string fullPathFiles)
        {
            /**Contar apariciones de una palabra en un fichero de texto wordcount.txt
            Se pide que:
            El usuario pueda introducir por consola una o varias palabras separadas por espacio, y le
            imprimiremos por pantalla cuántas ocurrencias para cada palabra hay en el texto. La
            simulació acaba amb l'acció END. **/

            string result = "";
            string fileToRead = "wordcount.txt";
            string filePath = fullPathFiles + fileToRead;

            //Reemplazamos las comas y puntos por espacios vacios para encontrar más fácilmente la palabra
            //También lo ponemos en minúsculas para que las mayúsculas no sean un problema.
            string[] txt = File.ReadAllText(filePath).Replace(",", "").Replace(".", "").ToLower().Split(' ');
            string palabra = "";

            if (File.Exists(filePath))
            {
                do
                {
                    //Cuenta las palabras dentro del documento.
                    int wordCounter = 0;
                    Console.WriteLine("\n" + "Introduce la palabra que quieras buscar: ");
                    palabra = Console.ReadLine().ToLower();

                    foreach (var item in txt)
                    {
                        if (palabra == item)
                        {
                            wordCounter++;
                        }
                    }

                    if (palabra != "end")
                    {
                        Console.WriteLine("La palabra " + palabra + " aparece " + wordCounter + " veces en el fichero");
                    }

                } while (palabra != "end");
            }

            else
            {
                Console.WriteLine($"No existe el fichero en este directorio");
            }

            return result;
        }

        public static string configFileLanguage(string fullPathFiles)
        {
            /** Queremos configurar nuestro programa utilizando un fichero de configuración: Fichero
            adjunto en el classRoom
            Crea un programa que utilice los valores del fichero config.txt y permita seleccionar el
            idioma en el que nos comunicaremos con el usuario **/

            string result = "";
            //los archivos a leer
            string fileToRead = "lang/config.txt";
            string fileCat = "lang/lng_cat.txt";
            string fileEsp = "lang/lng_es.txt";

            //el path de los archivos
            string filePath = fullPathFiles + fileToRead;
            string filePathEsp = fullPathFiles + fileEsp;
            string filePathCat = fullPathFiles + fileCat;

            //el texto de los archivos
            string[] txt = File.ReadAllText(filePath).Split(':');
            //txt[1] queda como Ana Idioma, así que con un Substring sacamos Ana.
            string cutText = txt[1].Substring(0, 3);
            string[] txtEsp = File.ReadAllText(filePathEsp).Split(' ');
            string[] txtCat = File.ReadAllText(filePathCat).Split(' ');

            if (File.Exists(filePath))
            {
                Console.WriteLine("Introduce el idioma: " + "\t");
                Console.Write("idioma ");
                string idioma = Console.ReadLine();

                //el idioma por defecto será "cat" o "es" según lo que ponga en config.txt. txt[2] es donde está el idioma
                string fileDefault = "lang/lng_" + txt[2] + ".txt";
                string filePathDefault = fullPathFiles + fileDefault;
                //texto de config.txt con el idioma por defecto
                string[] txtDefault = File.ReadAllText(filePathDefault).Split(' ');

                switch (idioma)
                {
                    case "es":
                        changeLanguage(filePathEsp, txtEsp, cutText);
                        break;
                    case "cat":
                        changeLanguage(filePathCat, txtCat, cutText);
                        break;
                    default:
                        changeLanguage(filePathDefault, txtDefault, cutText);
                        break;

                }

            }

            else
            {
                Console.WriteLine($"No existe el fichero en este directorio");
            }

            return result;
        }

        public static void changeLanguage(string filePath, string[] txtIdioma, string cutText)
        {
            //imprime el texto del fichero del idioma
            for (int i = 0; i < txtIdioma.Length - 1; i++)
            {
                Console.Write(txtIdioma[i] + " ");
            }
            //imprime Ana.
            Console.WriteLine(cutText);
        }

        public static string FindPaths()
        {
            string result = "";

            Console.WriteLine("Cual es el nombre del fichero a buscar?" + "\t");
            Console.Write("fichero ");
            string fichero = Console.ReadLine();
            Console.WriteLine("Dónde lo quieres buscar?");
            string newPath = Console.ReadLine();

            subDirectorios(newPath, fichero);


            return result;
        }

        public static void subDirectorios(string newPath, string fichero)
        {
            //Lista el fichero introducido del directorio que ha introducido el usuario y sus subdirectorios
            List<string> listaFichero = new List<string>(Directory.EnumerateFiles(newPath, fichero, SearchOption.AllDirectories));
            
            foreach(string file in listaFichero) 
            {
                Console.WriteLine("S'ha trobat el fitxer en: " + file);
                Console.WriteLine();
            }

        }

    }
}


