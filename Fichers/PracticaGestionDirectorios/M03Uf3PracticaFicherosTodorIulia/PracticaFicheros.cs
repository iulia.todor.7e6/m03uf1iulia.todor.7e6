﻿using System.Data;
using System.Diagnostics;
using System.IO;

internal class PracticaFicheros
{
    private static void Main(string[] args)
    {
        Menu();
    }

    public static void Menu()
    {
        string path = Path.GetFullPath(@"C:\Users\Colli\source\repos\");
        //string path = Path.GetFullPath(@"..\..\..");
        string directori = path + @"M03Uf3PracticaFicherosTodorIulia\";
        Directory.CreateDirectory(directori);

        PracticaFicheros p = new PracticaFicheros();

        string? opcio;
        do
        {
            Console.WriteLine("0. Sortir");
            Console.WriteLine("1. Ejercicio 1");
            Console.WriteLine("2. Ejercicio 2");
            Console.WriteLine("3. Ejercicio 3");
            Console.WriteLine("4. Ejercicio 4");

            opcio = Console.ReadLine();

            switch (opcio)
            {
                case "1":
                    p.Ejercicio1(directori);
                    break;
                case "2":
                    p.Ejercicio2(directori);
                    break;
                case "3":
                    p.Ejercicio3(directori);
                    break;
                case "4":
                    p.Ejercicio4(directori);
                    break;

                case "0":
                    Console.WriteLine("Bye");
                    break;
                default:
                    Console.WriteLine("Opción incorrecta");
                    break;
            }

            Console.ReadLine();

        } while (opcio != "0");
    }


    void Ejercicio1(string directori)
    {
        /**Demanem a l’usuari el nom d’un fitxer o d’una carpeta. Cal indicar si existeix o no. En cas d’existir indicar 
         * si es tracta d’un fitxer o d’un directori, i mostrar la ruta absoluta.**/

        Console.Write("Introduce el nombre: ");
        string? carpeta = Console.ReadLine();
        string path = directori + carpeta;
        string[] directories = Directory.GetDirectories(directori);
        string[] files = Directory.GetFiles(directori);

        if (Directory.Exists(path))
        {
            Console.WriteLine("Existe");

            //Si es un directorio
            foreach (string file in directories)
            {
                if (file == path)
                {
                    Console.WriteLine("Es un directorio: " + file);
                }
            }
        }

        else if (File.Exists(path))
        {
            Console.WriteLine("Existe");
            //Si es un fichero
            foreach (string file in files)
            {
                if (file == path)
                {
                    Console.WriteLine("Es un fichero: " + file);

                }
            }
        }


        else
        {
            Directory.CreateDirectory(path);
            Console.WriteLine("Creado" + path);
        }
    }

    void Ejercicio2(string directori)
    {
        /**Demanem a l’usuari un directori, hem de treure el llistat de les carpetes i fitxers del directori, 
         * com a cadenes, com a carpetes i com a fitxers, indicant si es tracta d’un directori <DIR>, o d’un 
         * fitxer <FILE>.**/

        Console.Write("Introduce un directorio: ");
        string? carpeta = Console.ReadLine();
        string path = directori + carpeta;

        if (Directory.Exists(path))
        {

            IEnumerable<string> files = Directory.GetFiles(path);
            IEnumerable<string> directories = Directory.GetDirectories(path);

            foreach (string file in directories)
            {
                Console.WriteLine("Directories: " + file);
            }

            foreach (string file in files)
            {
                Console.WriteLine("File: " + file);
            }

        }
        else
        {
            Directory.CreateDirectory(path);
            File.Create(path + ".txt");
            Console.WriteLine("Creado" + path);
        }
    }

    void changeName(string path, string directori, string carpeta)
    {

        Console.WriteLine("Qué nombre quieres ponerle?");
        string? newName = Console.ReadLine();


        if (newName == carpeta)
        {
            Console.WriteLine("Este directorio ya existe, no me vayas a poner dos directorios iguales");
        }
        else
        {
            Directory.Move(path, directori + newName);
            Console.Write("Nombre cambiado" + "\n");
        }


    }

    void deleteDirectory(string path)
    {
        Directory.Delete(path);
        Console.WriteLine("Directorio eliminado");
    }

    void createDirectory(string path)
    {
        Directory.CreateDirectory(path);
        Console.WriteLine("Creado" + path);
    }

    void Ejercicio3(string directori)
    {

        /**Demanem a l’usuari un directori origen, hem de poder crear un directori , esborrar-lo i/o canviar-li el nom.**/

        string? command = "a";

        do
        {
            Console.WriteLine("Introduce el directorio que quieras consultar: ");
            string? carpeta = Console.ReadLine();
            string path = directori + carpeta;


            if (Directory.Exists(path))
            {
                Console.WriteLine("Si quieres cambiarle el nombre al directorio pulsa N. Si quieres borrarlo pulsa D. Si quieres acabar pulsa E");
                Console.WriteLine("Si el directorio no existe tranqui, que ya te lo creo yo ;)");
                command = Console.ReadLine()?.ToUpper();

                switch (command)
                {
                    case "N":
                        changeName(path, directori, carpeta);
                        break;
                    case "D":
                        deleteDirectory(path);
                        break;
                    case "E":
                        Console.WriteLine("Adiós");
                        break;
                    default:
                        Console.WriteLine("Comando incorrecto");
                        break;

                }

            }

            else
            {
                createDirectory(path);
            }

        } while (command != "E");

    }

    void Ejercicio4(string directori)
    {
        string? carpeta = Console.ReadLine();
        string path = directori + carpeta;

        if (Directory.Exists(path))
        {
            DirectoryInfo carpetaInfo = new DirectoryInfo(path);
            DirectoryInfo[] directoryInfos = carpetaInfo.GetDirectories();
            FileInfo[] filesInfo = carpetaInfo.GetFiles("*", SearchOption.TopDirectoryOnly);


            Console.WriteLine("Tipo: " + "\t" + "\t" + "Tipo" + "\t" + "\t" + "Fecha modificación" + "\t" + "\t" + "Tamaño");

            foreach (DirectoryInfo item in directoryInfos)
            {
                Console.WriteLine(item.Name + "\t" + "\t" + item.Attributes + "\t" + item.LastWriteTime);
            }

            foreach (FileInfo item in filesInfo)
            {
                Console.WriteLine(item.Name + "\t" + item.Attributes + "\t" + "\t" + item.LastWriteTime + "\t" + "\t" + item.Length + " bytes");
            }

        }
        else
        {
            Directory.CreateDirectory(path);
            File.Create(path + ".txt");
            Console.WriteLine("Creado" + path);
        }
    }

}