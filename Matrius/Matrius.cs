﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Net.Http.Headers;
using System.Numerics;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Matrius
{
    class Matrius
    {
        /**Autor: Iulia Todor
         * Data: 24/11/2022
         * Descripcio: Donada la següent configuració del joc Enfonsar la flota, indica si a la posició (x, y) hi ha aigua o un vaixell (tocat)**/
        public static void SimpleBattleshipResult()
        {
            Console.WriteLine("Aquí tenemos un tablero con mar. También hay barcos, pero están ocultos");

            //Definimos una matriz de 7x7
            string[,] coordenades = new string[7, 7];

            

            //0 es mar
            //X son barcos

            //Imprime la matriz con todo mar (es decir, 0)
            for (int i = 0; i < coordenades.GetLength(0); i++)
            {
                for (int j = 0; j < coordenades.GetLength(1); j++)
                {
                    coordenades[i, j] = "0";
                    Console.Write(coordenades[i, j] + "\t");
                }
                Console.WriteLine();
            }

            //Definimos en qué coordenadas queremos que haya barcos
            coordenades[0, 1] = "X";
            coordenades[0, 2] = "X";
            coordenades[0, 6] = "X";
            coordenades[1, 2] = "X";
            coordenades[2, 6] = "X";
            coordenades[3, 1] = "X";
            coordenades[3, 2] = "X";
            coordenades[3, 3] = "X";
            coordenades[3, 6] = "X";
            coordenades[4, 4] = "X";
            coordenades[5, 4] = "X";
            coordenades[0, 6] = "X";


            //Para cuando el usuario quiera terminar
            string END = "a";

            Console.WriteLine("Dame unas coordenadas y te diré si allí hay agua o barcos");

            //Las coordenadas X Y
            int marx = -1;
            int mary = -1;

            do
            {
                //Para pedirle las coordenadas al usuario y vigilar que no ponga coordenadas fuera del rango
                do
                {
                    Console.WriteLine("Recuerda que es un tablero de 7x7, las coordenadas no pueden ser menores a 0 o mayores a 6");
                    Console.WriteLine("Dame las coordenadas X: ");
                    marx = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Dame las coordenadas Y: ");
                    mary = Convert.ToInt32(Console.ReadLine());
                } while (marx < 0 || mary < 0 || marx > 6 || mary > 6);

                //Para imprimir mar o tocat según las coordenadas
                if (coordenades[marx, mary] == "0")
                {
                    Console.WriteLine("mar");
                }

                else if (coordenades[marx, mary] == "X")
                {
                    Console.WriteLine("tocat");
                }

                Console.WriteLine("Si quieres terminar escribe END. Si quieres continuar pulsar enter");

                END = Console.ReadLine();


            } while (END != "END");

            Console.WriteLine("Adiós. Por si tienes curiosidad, este era el tablero completo");


            //Imprime el tablero completo
            for (int i = 0; i < coordenades.GetLength(0); i++)
            {
                for (int j = 0; j < coordenades.GetLength(1); j++)
                {
                    Console.Write(coordenades[i, j] + "\t");

                }
                Console.WriteLine();
            }

        }

        /**Autor: Iulia Todor
       * Data: 24/11/2022
       * Descripcio: Donada la següent matriu
        matrix = {{2,5,1,6},{23,52,14,36},{23,75,81,64}}

        Imprimeix la suma de tots els seus valors.
        **/

        public static void MatrixElementSum()
        {
            Console.WriteLine("Tenemos esta matriz:");

            //Declaramos la matriz
            int[,] matrix = new int[3, 4] { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 64 } };
            //El sumador irá sumando los valores de la matriz
            int sumador = 0;

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    //Imprimimos la matriz y a la vez el sumador va sumando los valores
                    Console.Write(matrix[i, j] + "\t");
                    sumador += matrix[i, j];
                }
                Console.WriteLine();
            }

            //Imprimimos el sumador
            Console.WriteLine("La suma de estas matrices es: " + sumador);

        }
        /**Autor: Iulia Todor
       * Data: 24/11/2022
       * Descripcio: Un banc té tot de caixes de seguretat en una graella, numerades per fila i columna del 0 al 3.
        Volem registrar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte.
        L'usuari introduirà parells d'enters del 0 al 3 quan s'obri la caixa indicada.
        Quan introdueixi l'enter -1, és que s'ha acabat el dia. Printa per pantalla el nombre de cops que s'ha obert.
        **/

        public static void MatrixBoxesOpenedCounter()
        {
            Console.WriteLine("Tenemos una serie de cajas. Vamos registrar en una matriz de 4x4 el número de veces que hayas abierto las cajas. Quiero que apuntes con las coordenadas qué cajas quieres abrir");

            //Declaramos la matriz 4x4
            int[,] cajas = new int[4, 4];
     
            //Las coordenadas XY
            int cajasx = 0;
            int cajasy = 0;


            do
            {

            Console.WriteLine("Introduce las coordenadas. Cuando quieras acabar introduce -1. En caso de introducir unas coordenadas incorrectas se te pedirá que las introduzcas de nuevo.");
             
                //Controla que el usuario introduzca las coordenadas X correctas.
                do
                {
                    Console.WriteLine("Las coordenadas X: ");
                    cajasx = Convert.ToInt32(Console.ReadLine());

                } while (cajasx < -1 || cajasx > 3);



                //Controla que el usuario introduzca las coordenadas Y correctas.
                do
                {
                    Console.WriteLine("Las coordenadas Y: ");
                    cajasy = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("-----------------------");
                } while (cajasy < -1 || cajasy > 3);

                //Al leer las coordenadas, caso de que alguna de las coordenadas sea -1 el bucle acabará.

                //Para que el número de la caja aumente. Está dentro de un if para que si el usuario introduce -1 no de error al salir de los límites
                if (cajasx != -1 && cajasy != -1)
                {
                    Console.WriteLine("Has introducido: " + cajasx + ", " + cajasy + ". Se han guardado los valores");
                    cajas[cajasx, cajasy]++;
                }

                else
                {
                    Console.WriteLine("Mostrando resultados: ");
                }


            } while (cajasx != -1 && cajasy != -1);

            //Imprime las veces que el usuario ha abierto cada caja
            for (int i = 0; i < cajas.GetLength(0); i++)
            {
                for (int j = 0; j < cajas.GetLength(1); j++)
                {
                    Console.Write(cajas[i, j] + "\t");
                }
                Console.WriteLine();
            }

        }
        /**Autor: Iulia Todor
       * Data: 24/11/2022
       * Descripcio: Donada la següent matriu
        matrix = {{2,5,1,6},{23,52,14,36},{23,75,81,62}}

        Imprimeix true si algún dels números és divisible entre 13, false altrement.
        **/

        public static void MatrixThereADiv13()
        {
            Console.WriteLine("Teniendo la siguiente matriz: ");

            //div13 empieza siendo false, y se volverá true si hay algún número divisible entre 13
            bool div13 = false;
            //

            //He cambiado un 23 por un 26 para tener más números divisibles entre 13. Así al imprimir no sale solo el 52.
            int[,] matrix = new int[3, 4] { { 2, 5, 1, 6 }, { 26, 52, 14, 36 }, { 23, 75, 81, 62 } };

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                    //div13 se vuelve true si hay algún número divisible entre 13
                    if (matrix[i, j] % 13 == 0)
                    {
                        div13 = true;
                    }
                }
                Console.WriteLine(); 
            }

            Console.WriteLine();

            Console.WriteLine("Hay algún número divisible entre 13? " + div13);

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    //Si hay algún número divisible entre 13, este se imprime al final
                    if (matrix[i, j] % 13 == 0)
                    {
                        Console.Write(matrix[i, j] + "\t");
                    }
                }
                Console.WriteLine();
            }

        }
        /**Autor: Iulia Todor
       * Data: 24/11/2022
       * Descripcio: Usant les imatges d'un satel·lit hem pogut fer raster o (mapa de bits) que ens indica l'alçada d'un punt concret d'un mapa. Hem obtingut la següent informació:
        map ={{1.5,1.6,1.8,1.7,1.6},{1.5,2.6,2.8,2.7,1.6},{1.5,4.6,4.4,4.9,1.6},{2.5,1.6,3.8,7.7,3.6},{1.5,2.6,3.8,2.7,1.6}}

        Digues en quin punt(x,y) es troba el cim més alt i la seva alçada   
        **/

        public static void HighestMountainOnMap()
        {
            Console.WriteLine("Teniendo estas alturas");

            //Alturamax es un valor que empieza siendo 0 y servirá para determinar la altura más alta
            double alturamax = 0;
            //Estas son las coordenadas para saber en qué posición se encuentra la altura más alta
            int coordenadasx = 0;
            int coordenadasy = 0;

            //Declaramos la matriz
            double[,] altura = new double[5, 5] { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };

            for (int i = 0; i < altura.GetLength(0); i++)
            {
                for (int j = 0; j < altura.GetLength(1); j++)
                {
                    Console.Write(altura[i, j] + "\t");
                    //Si el valor de la matriz es más alto que alturamax, esta pasará a ser el valor de la matriz. Así hasta conseguir la altura más alta
                    if (altura[i, j] > alturamax)
                    {
                        alturamax = altura[i, j];
                        //Las coordenadas se convertirán en las del valor más alto y se imprimirán al final
                        coordenadasx = i;
                        coordenadasy = j;
                    }
                }
                Console.WriteLine();
            }

            Console.WriteLine();

            Console.WriteLine("La altura más alta es: " + alturamax);
            Console.WriteLine("Se encuentra en las coordenadas " + coordenadasx + "," + coordenadasy);
        }
        /**Autor: Iulia Todor
       * Data: 24/11/2022
       * Descripcio: El govern britànic ens ha demanat que també vol accedir a les dades de l'exercici anterior i que les necessitaria en peus i no metres.
        Per convertir un metre a peus has de multiplicar els metres per 3.2808.
        Fes la conversió i imprimeix la matriu per pantalla.
        **/

        public static void HighestMountainScalechange()
        {
            Console.WriteLine("Vamos a cambiar los valores de la matriz de metros a pies:");

            //Volvemos a declarar la matriz
            double[,] altura = new double[5, 5] { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };

            for (int i = 0; i < altura.GetLength(0); i++)
            {
                for (int j = 0; j < altura.GetLength(1); j++)
                {
                    //Pasamos cada valor de metros a pies
                    altura[i, j] *= 3.2808;
                    //Redondeamos el valor para que la matriz no quede descolocada
                    altura[i, j] = Math.Round(altura[i, j]);
                    Console.Write(altura[i, j] + "\t");

                }
                Console.WriteLine();
            }
        }
        /**Autor: Iulia Todor
       * Data: 24/11/2022
       * Descripcio: Implementa un programa que demani dos matrius a l'usuari i imprimeixi la suma de les dues matrius.**/
        public static void MatrixSum()
        {
            Console.WriteLine("Dime de qué tamaño quieres la primera matriz");

            //Declaramos las filas y columnas de la primera matriz
            int tamanyoX1 = 0;
            int tamanyoY1 = 0;

            //Para que el usuario no introduzca valores no válidos para las filas y columnas
            do
            {
                Console.WriteLine("Recuerda que no puedes poner una matriz de tamaño negativo o 0. También preferiría que no pusieras una de tamaño 1; no te lo voy a impedir, pero sería bastante extraño" + "\n");
                Console.WriteLine("Número de filas: ");
                tamanyoX1 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Número de columnas:");
                tamanyoY1 = Convert.ToInt32(Console.ReadLine());
            }

            while (tamanyoX1 < 1 || tamanyoY1 < 1);

            //Declaramos la primera matriz con estas dimensiones
            int[,] tamanyo1 = new int[tamanyoX1, tamanyoY1];

            Console.WriteLine("Ahora dime los valores de la primera matriz");

            //Pedimos los valores al usuario
            for (int i = 0; i < tamanyo1.GetLength(0); i++)
            {
                for (int j = 0; j < tamanyo1.GetLength(1); j++)
                {
                    tamanyo1[i, j] = Convert.ToInt32(Console.ReadLine());
                }

                Console.WriteLine();
            }

            Console.WriteLine("Dime de qué tamaño quieres la segunda matriz");

            //Filas y columnas de la segunda matriz
            int tamanyoX2 = 0;
            int tamanyoY2 = 0;

            //Para que la segunda matriz tenga las mismas dimensiones que la primera
            do
            {
                Console.WriteLine("Recuerda que la segunda matriz tiene que tener el mismo tamaño que la primera");
                Console.WriteLine("Número de filas: ");
                tamanyoX2 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Número de columnas: ");
                tamanyoY2 = Convert.ToInt32(Console.ReadLine());

            } while (tamanyoX2 != tamanyoX1 || tamanyoY2 != tamanyoY1);

            //Declaramos la segunda matriz con estas dimensiones
            int[,] tamanyo2 = new int[tamanyoX2, tamanyoY2];

            Console.WriteLine("Ahora dime los valores de la segunda matriz");

            //Pedimos al usuario los valores de la segunda matriz
            for (int i = 0; i < tamanyo2.GetLength(0); i++)
            {
                for (int j = 0; j < tamanyo2.GetLength(1); j++)
                {
                    tamanyo2[i, j] = Convert.ToInt32(Console.ReadLine());
                }

                Console.WriteLine();
            }

            //Imprimimos las dos matrices
            Console.WriteLine("La primera matriz es:");


            for (int i = 0; i < tamanyo1.GetLength(0); i++)
            {
                for (int j = 0; j < tamanyo1.GetLength(1); j++)
                {
                    Console.Write(tamanyo1[i, j] + "\t");
                }
                Console.WriteLine();
            }

            Console.WriteLine("La segunda matriz es:");


            for (int i = 0; i < tamanyo2.GetLength(0); i++)
            {
                for (int j = 0; j < tamanyo2.GetLength(1); j++)
                {
                    Console.Write(tamanyo2[i, j] + "\t");
                }
                Console.WriteLine();
            }

            //Declaramos una matriz que sera la suma
            int[,] suma = new int[tamanyoX1, tamanyoY1];

            Console.WriteLine("Y la suma de las matrices es: ");

            for (int i = 0; i < tamanyo1.GetLength(0); i++)
            {
                for (int j = 0; j < tamanyo2.GetLength(1); j++)
                {
                    suma[i, j] = tamanyo1[i, j] + tamanyo2[i, j];
                }
            }

            //Imprimimos la suma
            for (int i = 0; i < tamanyo1.GetLength(0); i++)
            {
                for (int j = 0; j < tamanyo2.GetLength(1); j++)
                {
                    Console.Write(suma[i, j] + "\t");
                }
                Console.WriteLine();
            }

        }
        /**Autor: Iulia Todor
       * Data: 24/11/2022
       * Descripcio: Programa una funció que donat un tauler d'escacs, i una posició, ens mostri per pantalla quines són les possibles posicions a les que es pot moure una torre.
Primer caldrà llegir la posició de la torre, que simbolitzarem en el tauler amb el valor ♜ (fes copy & paste del símbol, és un string ja que ocupa dos chars), les posicions on aquesta es pugui moure les simbolitzarem amb el valor ♖ i la resta de posicions amb el valor x.
El moviment de la torre és el següent: Es pot moure al llarg d'una fila o d'una columna (però no successivament en una mateixa jugada); se la pot fer avançar tantes caselles com es vulgui.**/

        public static void RookMoves()
        {
            Console.WriteLine("Tenemos una torre en un tablero de ajedrez. Di en qué posición quieres la torre y te diré hacia dónde se puede mover");

            //Esta es la matriz del tablero de ajedrez     
            char[,] tablero = new char[8, 8];

            //Imprimimos X en todo el tablero
            for (int i = 0; i < tablero.GetLength(0); i++)
            {
                for (int j = 0; j < tablero.GetLength(1); j++)
                {
                    tablero[i, j] = 'X';
                    Console.Write(tablero[i, j] + "\t");
                }
                Console.WriteLine();
            }

            //El char es la letra que el usuario introduce para las coordenadas de la torre. Luego será cambiada a la posición X
            char letra = 'a';
            //Las coordenadas de la torre
            int posicionX = 0;
            int posicionY = 0;

            //Para que el usuario introduzca coordenadas válidas
            do
            {
                Console.WriteLine("Introduce unas coordenadas válidas para un tablero de ajedrez. La primera es una letra entre a y h minúsculas, la segunda un número entre 0 y 7");
                letra = Convert.ToChar(Console.ReadLine());
                posicionX = Convert.ToInt32(Console.ReadLine());
            } while ((int)letra > 'h' || posicionX > 8 || (int)letra < 'a' || posicionX < 1);

            //Según cual haya sido la letra, la posición X adoptará un cierto valor
            switch (letra)
            {
                case 'a':
                    posicionY = 1;
                    break;
                case 'b':
                    posicionY = 2;
                    break;
                case 'c':
                    posicionY = 3;
                    break;
                case 'd':
                    posicionY = 4;
                    break;
                case 'e':
                    posicionY = 5;
                    break;
                case 'f':
                    posicionY = 6;
                    break;
                case 'g':
                    posicionY = 7;
                    break;
                case 'h':
                    posicionY = 8;
                    break;

                default:
                    Console.WriteLine("Eso no es un valor válido");
                    break;
            }

            Console.WriteLine("Ahora vamos a ver los posibles movimientos de la torre");

            //Los # son los movimientos que puede hacer la torre. Los imprimimos en las posiciones X e Y
            for (int i = 0; i < tablero.GetLength(0); i++)
            {
                tablero[posicionX - 1, i] = '#';
                tablero[i, posicionY - 1] = '#';
            }

            //El 0 es el valor de la torre
            tablero[posicionX - 1, posicionY - 1] = '0';

            for (int i = 0; i < tablero.GetLength(0); i++)
            {
                for (int j = 0; j < tablero.GetLength(1); j++)
                {
                    Console.Write(tablero[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }
        /**Autor: Iulia Todor
       * Data: 24/11/2022
       * Descripcio: Donada una matriu quadrada, el programa imprimeix true si la matriu és simètrica, false en cas contrari.**/

        public static void MatrixSimetric()
        {

            Console.WriteLine("Escribe una matriz cuadrada y te diré si es simétrica o no");
            Console.WriteLine("Primero dime de qué tamaño la quieres");

            //Las coordenadas de la matriz
            int X = 0;
            int Y = 0;

            //Para que el usuario introduzca una matriz de valores válidos
            do
            {
                Console.WriteLine("Recuerda que una matriz cuadrada tiene el mismo número de filas que columnas. Y nada de valores negativos. También es preferible que sea un 2x2 o mayor; una matriz de 1x1 es bastante estúpida, por supuesto que va a ser simétrica");

                X = Convert.ToInt32(Console.ReadLine());
                Y = Convert.ToInt32(Console.ReadLine());


            } while (X != Y || X < 0 || Y < 0);

            //Declaramos la matriz
            int[,] matriz = new int[X, Y];

            Console.WriteLine("Ahora dime los valores de la matriz");

            for (int i = 0; i < matriz.GetLength(0); i++)
            {
                for (int j = 0; j < matriz.GetLength(1); j++)
                {
                    matriz[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }

            Console.WriteLine("Esta es tu matriz:");

            for (int i = 0; i < matriz.GetLength(0); i++)
            {
                for (int j = 0; j < matriz.GetLength(1); j++)
                {
                    Console.Write(matriz[i, j] + "\t");
                }
                Console.WriteLine();
            }

            //Value aumentará si la matriz no es simétrica
            int value = 0;

            for (int i = 0; i < matriz.GetLength(0); i++)
            {
                for (int j = 0; j < matriz.GetLength(1); j++)
                {
                    if (matriz[i, j] != matriz[matriz.GetLength(0) - i - 1, matriz.GetLength(1) - j - 1])
                    {
                        value++;
                    }
                }
            }

            if (value == 0)
            {
                Console.WriteLine("Es simétrica");
            }

            else
            {
                Console.WriteLine("No es simétrica");
            }
        }

        public static void Menu()
        {
            string opcio;
            do
            {
                Console.WriteLine("0. Sortir");
                Console.WriteLine("1. SimplesBattleshipResult");
                Console.WriteLine("2. MatrixElementSum");
                Console.WriteLine("3. MatrixBoxesOpenedCounter");
                Console.WriteLine("4. MatrixThereADiv13");
                Console.WriteLine("5. HighestMountainOnMap");
                Console.WriteLine("6. HighestMountainScalechange");
                Console.WriteLine("7. MatrixSum");
                Console.WriteLine("8. RookMoves");
                Console.WriteLine("9. MatrixSimetric");





                opcio = Console.ReadLine();

                switch (opcio)
                {
                    case "1":
                        SimpleBattleshipResult();
                        break;
                    case "2":
                        MatrixElementSum();
                        break;
                    case "3":
                        MatrixBoxesOpenedCounter();
                        break;
                    case "4":
                        MatrixThereADiv13();
                        break;
                    case "5":
                        HighestMountainOnMap();
                        break;
                    case "6":
                        HighestMountainScalechange();
                        break;
                    case "7":
                        MatrixSum();
                        break;
                    case "8":
                        RookMoves();
                        break;
                    case "9":
                        MatrixSimetric();
                        break;
                    case "0":
                        Console.WriteLine("Bye");
                        break;
                    default:
                        Console.WriteLine("Opción incorrecta");
                        break;
                }

                Console.ReadLine();

            } while (opcio != "0");
        }
        static void Main()
        {
            Menu();
        }

    }
}
