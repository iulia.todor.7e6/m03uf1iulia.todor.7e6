﻿/** Autor: Iulia Todor
 * Data: 11/10/2022
 * Descripció: Demana un enter a l'usuari. Imprimeix per pantalla tants punts com l'usuari hagi indicat**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número");
        //variable
        int num = Convert.ToInt32(Console.ReadLine());

        for (int i = 0; i <= num; i++)
        {
            //A medida que aumenta i, se van introduciendo puntos hasta llegar al número
            Console.WriteLine(".");
        }

    }
}