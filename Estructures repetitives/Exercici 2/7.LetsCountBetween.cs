﻿/** Autor: Iulia Todor
 * Data: 11/10/2022
 * Descripció: Printa per pantalla tots els valors que hi ha entre els dos valors introduits ordenats de menor a major**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce dos números y te imprimiré los números entre ellos");
        //variables
        int i = Convert.ToInt32(Console.ReadLine());
        int j = Convert.ToInt32(Console.ReadLine());
        //por si i es más pequeño que j
        if (i < j)
        {
            for (int k = i + 1; k < j; k++)
            {
                Console.WriteLine(k);
            }
        }
        //si i y j son iguales no hay cuenta que hacer

        //por si j es más pequeño que i
        else
        {
            for (int k = j + 1; k < i; k++)
            {
                Console.WriteLine(k);
            }
        }



    }
}