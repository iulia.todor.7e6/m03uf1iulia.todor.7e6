﻿/** Autor: Iulia Todor
 * Data: 11/10/2022
 * Descripció: L'usuari introdueix un enter (N) i es mostra per pantalla un compte enrere de N fins a 1**/


internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Pon un número y te pondré la cuenta atrás hasta 1");

        //variable
        int i = Convert.ToInt32(Console.ReadLine());

        //conforme j va disminuyendo hasta llegar a uno, se imprime por pantalla
        for (int j = i - 1; j > 0; j--)
        {
            Console.WriteLine(j);
        }

    }
}