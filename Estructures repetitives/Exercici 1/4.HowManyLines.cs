﻿/**Author: Iulia Todor
 * Data: 8/10/2022
 * Volem comptar quantes línies té un text introduït per l'usuari.L'usuari introduiex un text per pantalla 
 * que pot tenir salts de línia. Per indicar que ha acabat d'introduïr el text escriurà una línia amb 
 * la paraula clau END. Imprimeix el nombre de línies que ha introduït l'usuari (sense contar la END).**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe algo y te diré cuantas líneas tiene. Introduce END para terminar");

        //Variables
        int nl = 0;
        string s = Console.ReadLine();

        while (s != "END")
        {
            //el número de líneas aumentará hasta que el usuario introduzca END
            nl++;
            s = Console.ReadLine();
        }
        Console.WriteLine("Numero líneas: " + nl);

    }
}