﻿/**Author: Iulia Todor
 * Data: 8/10/2022
 * Descripció: Mostra per pantalla tots els números fins a un enter entrat per l'usuari.**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número");
        //variables
        int num = Convert.ToInt32(Console.ReadLine());
        int j = 0;

        //Durante el bucle, j irá aumentando y se imprimirá por pantalla
        while (j < num)
        {
            j++;
            Console.WriteLine(j);
        }
    }
}