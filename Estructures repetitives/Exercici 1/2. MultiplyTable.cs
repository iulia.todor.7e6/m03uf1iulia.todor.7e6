﻿/**Author: Iulia Todor
 * Data: 8/10/2022
 * Descripció: Volem printar les taules de multiplicar. L'usuari introdueix un enter. Mostra per pantalla 
 * la taula de multiplicar del número introduït:.**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Escribe un número y te pondré su tabla de multiplicar:");
        //variables
        int i = Convert.ToInt32(Console.ReadLine());
        int n = 1;

        while (n < 10)
        {
            if (i < 0)
            {
             Console.WriteLine(n + "*" + "(" + i + ")" + "=" + (i * n));
            }
            
            else if (i == 0)
            {
                Console.WriteLine("¿Para qué querrías eso?");
                break;
            }
            else
            { 
             Console.WriteLine(n + "*" + i + "=" + (i * n));
            }

            n++;
        }
    }
}