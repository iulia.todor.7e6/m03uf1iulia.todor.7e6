﻿/**Author: Iulia Todor
 * Data: 8/10/2022
 * Demanar a l'usuari un enter entre 1 i 5. Si introdueix un número més gran o més petit, torna-li a demanar.
Mostra per pantalla: El número introduït: 3, (en cas de que l'usuari hagi introduït un 3) substituint el 
3 pel número..**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número entre 1 y 5");

        int i = Convert.ToInt32(Console.ReadLine());

        do
        {
            Console.WriteLine("Esto no. Prueba otra vez");
            i = Convert.ToInt32(Console.ReadLine());  
        } while (i < 1 || i > 5);
        Console.WriteLine("El número introducido: " + i);





    }
}