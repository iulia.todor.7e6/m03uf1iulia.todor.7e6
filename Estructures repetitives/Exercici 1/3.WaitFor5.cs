﻿/**Author: Iulia Todor
 * Data: 8/10/2022
 * Descripció: Farem un programa que demana repetidament a l'usuari un enter fins que entri el número 5. 
 * L'usuari introdueix enters. Quan introdueixi el 5 imprimeix 5 trobat!.**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un 5");
        //variable
        int i = Convert.ToInt32(Console.ReadLine());
        while (!(i == 5))
        {
            if (i == -5)
            {
                Console.WriteLine("¿Te crees muy listo, eh?");
                i = Convert.ToInt32(Console.ReadLine());
            }
            //El bucle no se detiene hasta que el usuario no introduce un 5
            else
            {
                Console.WriteLine("Esto no es un 5");
                Console.WriteLine("Ánimo, no es tan difícil");
                i = Convert.ToInt32(Console.ReadLine());
                }   
        }

        Console.WriteLine("Esto sí");



    }
}