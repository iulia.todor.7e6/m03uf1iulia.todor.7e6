﻿/** Autor: Iulia Todor
 * Data: 19/10/2022
 * Descripció: Ens han demanat que fem un programa per un torneig de Go. Necessitem fer un 
 * programa que donada una llista de puntuacions, ens digui qui és el guanyador del torneig. 
 * L'usuari introduirà el nom del participant i la puntuació, cada un en una línia. 
 * Quan ja no hi hagi més participants entrara la paraula clau END.
 * Imprimeix per pantalla el guanyador del concurs i els punts obtinguts.**/

internal class GoTournamentGreatestScore
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce los resultados del torneo de Go. Recuerda que una vez hayas terminado debes escribir END");

        //variables
        string END = "";
        string jugador;
        int puntuación;
        string jugadormax = "a";
        int puntmax = 0;
  
        do
        {
            
            Console.WriteLine("Introduce el jugador");
            jugador = (Console.ReadLine());
            Console.WriteLine("Introduce la puntuación");
            puntuación = Convert.ToInt32(Console.ReadLine());
            //De esta forma la puntuación mayor siempre pasará a ser la más grande, y su jugador el ganador
            if  (puntuación > puntmax)
            {            
            
                puntmax = puntuación;
                jugadormax = jugador;
            }

            Console.WriteLine("Para terminar pulsa END");
            END = Console.ReadLine();
            

            } while (END != "END");

        Console.WriteLine("Ganador: " + jugadormax + ", con: " + puntmax);

        
        

        

    }
}