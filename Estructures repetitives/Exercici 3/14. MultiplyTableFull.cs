﻿

using System.Globalization;
/** Autor: Iulia Todor
* Data: 20/10/2022
* Descripció: Imprimeix les taules de multiplicar en forma de taula**/
internal class Program
{
    private static void Main(string[] args)
    {
      Console.WriteLine("Aquí tienes, chuletilla para el examen de tercero de primaria :p");

        //i será el primer factor y se irá incrementando hasta 9. 
        for (int i = 1; i < 10; i++)
        {
            /** dentro hay otro for con el segundo factor: j. Este también se irá incrementando,
             * y cuando llegue a 10 el segundo for terminará. Entonces volverá al primer for,
             * donde \n imprimirà una nueva línea. i volverá a aumentar, entrará en el segundo for
             * y j aumentará, y así sucesivamente. El primer for terminará cuando i llegue a 10.
             * **/
            for (int j = 1; j < 10; j++)
            {
                 /** Vamos a poner un ejemplo con la primera y segunda vuelta 
                  * Primera vuelta: En el primer for, i=1. Entra en el segundo for
                  * En el segundo for: j=1. La consola imprime i*j, que es igual a 1.
                  * Gracias a \t, se separan por filas. i seguirá siendo 1 hasta que
                  * acabe el segundo for. El segundo for continúa hasta que llega a 10,
                  * pero antes de que pase j irá aumentando, haciendo 1*2, 1*3, etc. e 
                  * imprimiéndolo.
                  * Segunda vuelta: j ha llegado a 10, así que sale del segundo for y al volver
                  * al primer for, \n imprime la primera vuelta. Hay un salto de línea.
                  * Volvemos al inicio del primer for, donde i aumenta y es igual a 2. 
                  * Entramos en el segundo for, donde j vuelve a ser 1 (ya que había terminado),
                  * así que hace el mismo proceso, pero con i siendo 2: 2*1, 2*2, 2*3... Y así 
                  * hasta terminar con todo.**/

                 Console.Write(i*j + "\t");     
            }
            //Imprimirá una nueva línea
            Console.Write("\n");
        }
       //Lo siento por esos comentarios, realmente necesitaba entenderlo
            


        }

        
    
    
    
}
