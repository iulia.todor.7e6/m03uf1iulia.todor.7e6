﻿/** Autor: Iulia Todor
 * Data: 19/10/2022
 * Descripció: L'usuari introdueix dos enters, la base i l'exponent.
 * Imprimeix el resultat de la potencia (sense usar la llibreria math)**/

internal class ManualPow
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Vamos a hacer una potencia");
        Console.WriteLine("Introduce la base");

        //variables
        int numbase = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Introduce el exponente");
        int exp= Convert.ToInt32(Console.ReadLine());
  
         //de esta forma numbase se multiplicará por la potencia continuamente,
         //la cual equivale a la base introducida
        int potencia = numbase;
        
     
        //conforme i vaya aumentando, la base se irá multiplicando por la potencia (esta
        //no irá aumentando porque es la base original
        for (int i = 1; i < exp; i++)
        {

           
            potencia *= numbase;
            
            
            
        }
        Console.WriteLine("El resultado es: " + potencia);

    }
}