﻿/** Autor: Iulia Todor
 * Data: 11/10/2022
 * Descripció: L'usuari introdueix dos valors enters, el final i el salt. Escriu tots els numeros 
 * des de l'1 fins al final, amb una distància de salt**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número y un salto. Haremos una cuenta atrás");
        //variables
        int num = Convert.ToInt32(Console.ReadLine());
        int salto = Convert.ToInt32(Console.ReadLine());
        int result = 1;

        //si el salto es mayor al número se intercambian las variables
        if (salto > num)
        {
            Console.WriteLine("Te he intercambiado las variables :)");
            int contenedor = num;
            num = salto;
            salto = contenedor;
            Console.Write(result + " ");
        }
        //Se imprime el resultado porque de esta forma empezamos con un 1
        else
        {
            Console.Write(result + " ");
        }

        do
        {
            //mientras el resultado sea menor al número, este se irá sumando al salto
            result += salto;
            //solo se imprimirá si es menor que el número, de esta forma no superará al número
            if (result <= num)
            {
                Console.Write(result + " ");
            }

        } while (result < num);

    }
}