﻿/**Author: Iulia Todor
 * Data: 8/10/2022
 * Descripció: Donada una llista de lletres, imprimeix únicament les vocals que hi hagi. L'entrada consta 
 * de dues parts: Primer s'indica la quantitat de lletres. A continuació venen les lletres separades per espais en blanc. Imprimeix totes les que són vocals**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce el número de letras que quieres");

        //variables
        int num = Convert.ToInt32(Console.ReadLine());
        char input;
        string vocales = "";
        int numvocales = 0;

        Console.WriteLine("Ahora introduce las letras:");

        //Mientras i sea menor al número, podremos seguir añadiendo letras. Si esta letra es una vocal,
        //numvocales aumentará y la vocal aparecerá al final
        for (int i = 0; i < num; i++)
        {

            input = Convert.ToChar(Console.ReadLine());

            switch (input)
            {
                case 'A':
                case 'a':
                    vocales += input;
                    numvocales++;
                    break;
                case 'E':
                case 'e':
                    vocales += input;
                    numvocales++;
                    break;
                case 'I':
                case 'i':
                    vocales += input;
                    numvocales++;
                    break;
                case 'O':
                case 'o':
                    vocales += input;
                    numvocales++;
                    break;
                case 'U':
                case 'u':
                    vocales += input;
                    numvocales++;
                    break;
                default:
                    break;
            }  
            
        }
        //No consigo que las vocales salga separadas
        Console.WriteLine("El número total de vocales es: " + numvocales + ". Y estas son: " + "\n" + vocales);
       
    }
}






