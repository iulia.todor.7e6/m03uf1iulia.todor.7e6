﻿/** Autor: Iulia Todor
* Data: 20/10/2022
* Descripció: S'imprimeix una piràmide d'altura N de #**/

internal class Piramid
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número y te haré una pirámide de #");
        //variable
        int sharp = Convert.ToInt32(Console.ReadLine());
        //Conforme aumenta j, imprime un #. Una vez acaba su for, i imprime un salto de línea
        for (int i = 1; i <= sharp; i++)
        {
            for(int j =1; j <= i; j++) 
            {
                Console.Write("#");
               
            }

            Console.Write("\n");
        }
           

    }
}