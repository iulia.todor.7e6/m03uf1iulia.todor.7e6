﻿

using System.Diagnostics.CodeAnalysis;
/** Autor: Iulia Todor
* Data: 20/10/2022
Descripció: Fer un programa que llegeixi un nombre enter positiu n i escrigui la suma de les seves xifres. 
Per exemple les xifres del nombre 456 sumen (4+5+6) = 15**/
internal class SumMyNumbers
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número y te sumaré las cifras");

        //variables
        int num= Convert.ToInt32(Console.ReadLine());
        int suma = 0;

        /** Para este cálculo hay que separar los números e ir sumàndolos uno a uno
         * El bucle parará cuando num sea 0, ya que no podrá separar sus cifras más.
         * Como no sé cuantas operaciones harán falta para que num sea 0, usaré while.
         * Tenemos también la variable suma, que irá acumulando la suma**/
        while (num != 0)
        {
            //Primero vamos a hacer un módulo del número con 10 para conseguir el residuo.
            // Este residuo será el último número introducido por el usuario (por ejemplo,
            // si introduce 123, conseguiremos 3. Este se convertirá en la suma**/

            suma += num % 10; 

            //Ahora tomaremos el resultado de la división, que será el número resultante
            //sin su último dígito (tomando el ejemplo anterior, tendremos 12.
            num = num / 10;

            //Esto se irá repitiendo en el bucle hasta que el número sea 0.
            //En una segunda ronda usando 123, la suma se convertiría en 5 (3+2, porque
            //el módulo de 12 y 10 sería 2). El número se convertiría en 1, y así hasta
            //el final.
        }
            Console.WriteLine("El resultado es: " + suma);


    }
}