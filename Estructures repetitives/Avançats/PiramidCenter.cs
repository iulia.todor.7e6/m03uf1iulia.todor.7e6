﻿/** Autor: Iulia Todor
* Data: 22/10/2022
* Descripció: S'imprimeix una piràmide d'altura N de # centrada**/
internal class PiramidCenter
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número y te haré una pirámide AÚN más chula :)");
        //variable
        int sharp = Convert.ToInt32(Console.ReadLine());
        //Una vez termina el ciclo de for, este introduce un salto de línea
        for (int i = 1; i <= sharp; i++)
        {
            //dentro hay dos for anidados dentro del primero. El primero se encarga del centrado, y será
            //más pequeño conforme baja la pirámide
            for (int j = 1; j <= sharp-i+1; j++)
            {
                
                Console.Write(" ");
            }
            //Este for imprime los #. Cuanto más grande se haga i, más # habrá
            for (int k = 1; k <= i; k++)
            {
                    Console.Write("#" + " ");
            }

            Console.Write("\n");
        }
    }
}