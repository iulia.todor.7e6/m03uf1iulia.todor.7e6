﻿/** Autor: Iulia Todor
* Data: 22/10/2022
* Descripció: Calcula el factorial d'un valor **/

using System.Globalization;

internal class Factorial
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número y te calcularé su factorial");

        //variable
        int num = Convert.ToInt32(Console.ReadLine());
        int fact = 1;

        //i será la variable que irá incrementando hasta llegar al número que ha dado el usuario
        for (int i = 1; i <= num; i++)
        {
            //fact al principio será uno, pero conforme i aumente, esta se multiplicará por fact y también
            //aumentará. Por ejemplo, si num es 4: 
            //Primera vuelta: 1*1 = 1
            //Segunda vuelta 1*2 = 2
            //Tercera vuelta 2*3 = 6
            //Cuarta vuelta 6*4 = 24

            fact *= i;
           
            }
        Console.WriteLine(fact);
            
        }
    }