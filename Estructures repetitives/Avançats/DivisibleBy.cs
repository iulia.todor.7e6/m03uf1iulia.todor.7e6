﻿/** Autor: Iulia Todor
* Data: 22/10/2022
* Descripció: Imprimeix tots els nombres enters divisibles per 3 que hi ha entre A i B (inclusiu).**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Dame dos números y te diré qué números divisibles entre 3 hay entre ellos");

        //variables
        int num1 = Convert.ToInt32(Console.ReadLine());
        int num2 = Convert.ToInt32(Console.ReadLine());

        //Por si num 1 es mayor
        if (num1 > num2)
        {
            Console.WriteLine("Te he intercambiado las variables :)");
            int c = num2;
            num2 = num1;
            num1 = c;

        }

       //Para que no hayan números divisibles entre 3 mayores a num2
        for (int i = 0; i <= num2; i++)
        {
            //Para que no hayan números divisibles entre 3 menores que num1
            if (i > num1)
            {
                //Los números divisibles entre 3
                if (i % 3 == 0)
                {
            
                Console.Write(i + ", ");
                }
            }
               
            
        }

        


    }
}