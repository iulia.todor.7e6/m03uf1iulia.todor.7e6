﻿/** Autor: Iulia Todor
* Data: 22/10/2022
* Descripció: Implementa un programa que mostre true si el nombre introduït és primer, false en qualsevol altre cas**/

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número y te diré si es primo o no");

        int num = Convert.ToInt32(Console.ReadLine());

        int divisor = 0;

        if (num == 0 || num == 1)
        {
            Console.WriteLine("0 y 1 no son primos");
        }

     
        for (int i = 1; i <= num; i++)
        {
        //un número primo solo puede ser divisible entre si mismo y 1. Si el módulo del número e i es 0, el
        //divisor aumentará
            if (num % i == 0) 
            {
                divisor++;
            }  
        }

        //como solo hay dos casos en que el módulo puede ser 0, si divisor es igual a dos significa que el número
        //es primo
        if (divisor == 2)
        {
            Console.WriteLine(true);
        }
        
        else
        {
            Console.WriteLine(false);
        }
        

        
        
    }
}