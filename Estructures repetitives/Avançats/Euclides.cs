﻿/** Autor: Iulia Todor
* Data: 22/10/2022
* Descripció: Implementa l'algorisme d'Euclides, que calcula le màxim comú divisor de dos nombres positius.**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Dame dos números y te daré su mcd");

        //variables
        int num1 = Convert.ToInt32(Console.ReadLine());
        int num2 = Convert.ToInt32(Console.ReadLine());
        int residuo = num2 % num1;

        //Por si num 1 es mayor
        if (num1 > num2)
        {
            Console.WriteLine("Te he intercambiado las variables (ya te vale...)");
            int c = num2;
            num2 = num1;
            num1 = c;
        }
        
        //Si el residuo es 0 significa que num1 es el mcd
        else if (residuo == 0)
        {
            Console.WriteLine(num1);
        }

        //Según el algoritmo de Euclides, el divisor de la operación que tenga como residuo 0 será el MCD.
        //El número 1 (el primer divisor) se irá dividiendo entre el residuo hasta que su módulo sea 0
        else
        {
            do
            {
                num1 = num1 % residuo;
            }
            while (num1 > 0);
        {
                
             Console.WriteLine(residuo);
        }
            
        }
              


            
        }

}
