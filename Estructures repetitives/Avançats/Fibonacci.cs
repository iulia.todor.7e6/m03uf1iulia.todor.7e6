﻿/** Autor: Iulia Todor
* Data: 22/10/2022
* Descripció: Calcula el factorial d'un valor **/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Dame un número y te diré su sucesión de Fibonacci");

        //variables
        int num = Convert.ToInt32(Console.ReadLine());
        int primero = 0;
        int segundo = 1;
        int siguiente;

        for (int i = 0; i <= num; i++)
        {
            //de esta forma conseguiremos el 0 y el 1 iniciales
            if (i <= 1)
            {
                siguiente = i;
            }
            /**harán una suma sucesiva. Por ejemplo, primero y segundo son 0 y 1 respectivamente. Al sumar, 
            siguiente será 1. Los valores se intercambiarán: primero será 1, segundo y siguiente también.
            Repitamos el paso. Primero + segundo será 2 (siguiente). Los valores se intercambiarán: primero
            será 1 y segundo y siguiente 2. Repitamos: primero más segundo será 3. Al intercambiar primero
             será 2, segundo y siguiente serán 3; la siguiente suma daría 5 Así aumentará de forma exponencial**/
            
            else {
                siguiente = primero + segundo;
                primero = segundo;
                segundo = siguiente;
            }
                Console.Write(siguiente + "\t"); 
            }
            
        }


    }
