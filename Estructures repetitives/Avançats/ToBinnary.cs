﻿/** Autor: Iulia Todor
* Data: 22/10/2022
* Descripció: Escriu un programa que llegeixi un nombre natural més petit que 256 i 
* escrigui la seva representació en binari.**/
internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Introduce un número menor de 256 y te lo pasaré a binario");
        //variable
        int num = Convert.ToInt32(Console.ReadLine());

        while (num > 0)
        {
            //por si el número es mayor a 256. Ese texto en binario es un texto de verdad,
            //es un mensaje súper especial
           if (num > 256)
             {
                num = 0;
                Console.WriteLine("01101000 01110100 01110100 01110000 01110011 00111010 00101111 00101111 01110111 01110111 01110111 00101110 01111001 01101111 01110101 01110100 01110101 01100010 01100101 00101110 01100011 01101111 01101101 00101111 01110111 01100001 01110100 01100011 01101000 00111111 01110110 00111101 01101101 01000011 01100100 01000001 00110100 01100010 01001010 01000001 01000111 01000111 01101011 00100110 01100001 01100010 01011111 01100011 01101000 01100001 01101110 01101110 01100101 01101100 00111101 01110011 01110111 01100101 01100101 01110100 01100010 01101100 01110101 01100101 00101110");
            } 
            //para hacer la división y sacar el 0 o 1
           else {
                int residuo = num % 2;
                num /= 2;
                Console.Write(residuo);
                //la verdad es que no he sabido hacer para que el binario aparezca
                //de abajo a arriba de la división
            }
        }   


    }
}